import pandas as pd
import matplotlib.pyplot as plt

FILE_DIR_TOUCHPAD = "data/Signatures-Touchpad/"
FILE_DIR_MOBILE = "data/Signatures-Mobile/"
FILE_DIR_MOBILE_LARGE = "data/Signatures-Mobile-New/"
FILE_DIR_MOBILE_OTHER = "data/Signatures-Mobile-Non-S8/"

FILE_DIR = FILE_DIR_TOUCHPAD
FILE_NAME = "sig0-Linus-Heimböck-1584454110690-true-1236-898-296-292-AcerAspireVN7571G573Q.csv"

# x,y,strokenumber,timestamp -> touchpad
signature = pd.read_csv(FILE_DIR + FILE_NAME)

print(signature.info())
print(signature.describe())

# Signature would be mirrored because y axis starts at the top left corner for screen coords, but bottom left here
signature.plot(x='x', y='y', kind='line')
plt.gca().invert_yaxis()
plt.title("Signature image (X/Y, inverted)")
plt.show()

# The x axis over time
signature.plot(x='timestamp', y='x', kind='line')
plt.title("X axis values over time")
plt.show()

# The y axis over time - again, this would be mirrored because of different coordinate systems
signature.plot(x='timestamp', y='y', kind='line')
plt.gca().invert_yaxis()
plt.title("Y axis values over time")
plt.show()

# Distribution of x coordinates
ax = signature['x'].plot.hist(bins=20)
ax.set_xlabel("X coordinates")
plt.title("Distribution of X values")
plt.show()

# Distribution of y coordinates
ax = signature['y'].plot.hist(bins=20)
ax.set_xlabel("Y coordinates")
plt.title("Distribution of Y values")
plt.show()