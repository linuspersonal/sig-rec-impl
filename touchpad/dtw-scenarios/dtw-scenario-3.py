from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output
from util.dynamic_time_warping import OUTPUT_DIR_TOUCHPAD, FILE_DIR_TOUCHPAD, FILE_NAME_TOUCHPAD, \
    FEATURES_XY

import pandas as pd

SKILLED_FORGERIES = False

df_touchpad = pd.read_csv(FILE_DIR_TOUCHPAD + FILE_NAME_TOUCHPAD)

# ----------------- Scenario 3 -------------------
dir_name = OUTPUT_DIR_TOUCHPAD + "dtw-scenario-3/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"

file_name = "dtw-3-sitting-xy.txt"
result = classify_with_dtw(df_touchpad, FEATURES_XY,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_touchpad, result)
write_dtw_output(dir_name, file_name, output, True)
