import pandas as pd
import numpy as np

from scipy.signal import butter, filtfilt
from util.generate_dataframe import generate_dataframe, pixels_to_reference, SCREEN_WIDTH, SCREEN_HEIGHT, \
    pixel_speed_to_reference
from util.process_and_train import normalize_colwise
from util.resample_dataframe import resample_fixed

FILE_DIR = "data/dataset-touchpad/"

SAMPLING_RATE = 10      # Hz of the var. length signature CSV's
TARGET_DURATION = 5     # Duration of the output signatures after stretching/crunching
SIGNATURE_SIZE = SAMPLING_RATE * TARGET_DURATION    # The amount of samples per signature

REFERENCE_WIDTH = 2000      # Transform all signature pixel coordinates to this reference width
REFERENCE_HEIGHT = 1000     # Transform all signature pixel coordinates to this reference height


def feature_extraction(signature: pd.DataFrame, file_params: [str]) -> pd.DataFrame:
    # Extract duration, which would be lost after resampling, and remove timestamp
    duration = signature['timestamp'].max()
    signature.drop(['timestamp'], axis=1, inplace=True)

    # Interpolate x/y
    cols_to_interpolate = ['x', 'y']
    signature[cols_to_interpolate] = signature[cols_to_interpolate].interpolate()

    # Both filtering only raw values and filtering raw and processed ones should be fine
    cols_to_filter = ['x', 'y']
    b, a = butter(4, 0.8, btype='low')
    signature[cols_to_filter] = filtfilt(b, a, signature[cols_to_filter], axis=0)

    # Time series feature extraction has to happen before resampling
    # See paper: 2008-dynamic-signature-verification-portable-devices
    # For derivates, see 2014-online-signature-verification-mobile-devices

    # Speed (derivate) - this is used for subsequent calculations
    signature["dX"] = (signature["x"] - signature["x"].shift()) * 50
    signature.loc[0, "dX"] = 0
    signature["dY"] = (signature["y"] - signature["y"].shift()) * 50
    signature.loc[0, "dY"] = 0

    # Normalize speed to mm/s - can be done, but not necessary, according to findling
    signature = pixel_speed_to_reference(signature, file_params, ['dX', 'dY'])

    # Acceleration
    # Speed/Acceleration can be calculated with vX/vY or dX/dY, doesn't really matter - but stick to one everywhere.
    # Consistency with touchpad might be better if self-calculated; Android might be more exact.
    signature["ddX"] = np.abs(signature["dX"]) - np.abs(signature["dX"].shift())
    signature.loc[0, "ddX"] = 0
    signature["ddY"] = np.abs(signature["dY"]) - np.abs(signature["dY"].shift())
    signature.loc[0, "ddY"] = 0

    # Path-tangent angle
    signature["pta"] = np.arctan(np.abs(signature["dY"]) / np.abs(signature["dX"]))
    signature["dpta"] = signature["pta"] - signature["pta"].shift()

    # Path velocity magnitude
    signature["pvm"] = np.sqrt(np.abs(signature["dY"]) + np.abs(signature["dX"]))
    signature["dpvm"] = signature["pvm"] - signature["pvm"].shift()
    # Min Max ratio
    signature["pvmr5"] = signature["pvm"].rolling(5).min() / signature["pvm"].rolling(5).max()

    # Log curvature radius - Can cause infinity by division by 0, that's why 0.1 is added
    signature["lcr"] = np.log10(signature["pvm"] / np.abs(signature["dpta"] + 0.1))
    signature["dlcr"] = signature["lcr"] - signature["lcr"].shift()

    # Total acceleration magnitude
    signature["tam"] = np.sqrt(np.power(signature["dpvm"], 2) +
                               np.power(signature["pvm"], 2) * np.power(signature["pta"], 2))
    signature["dtam"] = signature["tam"] - signature["tam"].shift()

    # Angle of consecutive samples - do this before any normalizations on the pixel size/shape to preserve angles
    signature["acs"] = np.arctan((signature["y"] - signature["y"].shift()) /
                                 (signature["x"] - signature["x"].shift()))
    signature["dacs"] = signature["acs"] - signature["acs"].shift()
    signature["sinacs"] = np.sin(signature["acs"])
    signature["cosacs"] = np.cos(signature["acs"])

    # Stroke length to width ratio
    # Causes division by 0 in some cases, when min and max are same - that's why 0.1 is added (not in original formula)
    signature["ltwr5"] = np.sqrt(
        np.power(signature["x"] - signature["x"].shift(), 2) +
        np.power(signature["y"] - signature["y"].shift(), 2)
    ) / (signature["x"].rolling(5).max() - signature["x"].rolling(5).min() + 0.1)
    signature["ltwr7"] = np.sqrt(
        np.power(signature["x"] - signature["x"].shift(), 2) +
        np.power(signature["y"] - signature["y"].shift(), 2)
    ) / (signature["x"].rolling(7).max() - signature["x"].rolling(7).min() + 0.1)

    # Extract statistical features
    stroke_count = signature['strokenumber'].max() + 1  # Starts at 0
    sample_count = signature.shape[0]

    # Make x/y values relative to screen size
    signature['x'] = signature['x'] / int(file_params[SCREEN_WIDTH])  # X size of screen
    signature['y'] = signature['y'] / int(file_params[SCREEN_HEIGHT])  # Y size of screen

    # Start- and end-values relative to screen size, so between 0 and 1
    start_x = signature['x'][0]
    start_y = signature['y'][0]
    end_x = signature['x'][sample_count - 1]
    end_y = signature['y'][sample_count - 1]

    min_x = signature['x'].min()
    min_y = signature['y'].min()
    max_x = signature['x'].max()
    max_y = signature['y'].max()

    width_x = max_x - min_x
    height_y = max_y - min_y
    size = width_x * height_y

    max_dX = signature['dX'].max()
    max_dY = signature['dY'].max()

    # Calculate means of all values - only makes sense when all signatures are in same ref coord system (normalized)
    # Do this before min-max-scaling, so that the position of the signature on screen is preserved
    cols_to_mean = ['x', 'y', 'dX', 'dY']
    means = signature[cols_to_mean].mean()
    stdevs = signature[cols_to_mean].std()
    skews = signature[cols_to_mean].skew()
    kurtoses = signature[cols_to_mean].kurtosis()

    # Move signature start point to 0,0 in coordinate system
    signature['x'] -= start_x
    signature['y'] -= start_y

    # Resample to fixed size
    signature = resample_fixed(signature, SIGNATURE_SIZE)

    # Add finger up/down column from stroke number column and remove
    signature['fc'] = signature['strokenumber'].notna().astype('int')
    signature.drop(['strokenumber'], axis=1, inplace=True)

    # Fill the first, uncalculatable nan's with means - there are no other Na's at this point anymore
    signature.fillna(signature.mean(), inplace=True)

    # Normalize the values of each signature - per time series, not per column (except for global features)
    # Maybe use median - MAD normalization - use if performs the same, according to findling
    signature = normalize_colwise(signature, 'fc')

    # Combine all rows of the signature into one sample, with each field being its own feature
    signature = signature.transpose()
    signature = signature.stack(dropna=False)  # Combine into one row
    signature.index = ['{}_{}'.format(*c) for c in signature.index]

    # Add means, stdevs, skews and kurtoses to series
    means = means.add_prefix('m')
    signature = signature.append(means)
    stdevs = stdevs.add_prefix('sd')
    signature = signature.append(stdevs)
    skews = skews.add_prefix('sk')
    signature = signature.append(skews)
    kurtoses = kurtoses.add_prefix('kt')
    signature = signature.append(kurtoses)

    # Add the data gained from analysis to series
    signature['duration'] = duration
    signature['nrStrokes'] = stroke_count
    signature['startX'] = start_x
    signature['startY'] = start_y
    signature['endX'] = end_x
    signature['endY'] = end_y
    signature['minX'] = min_x
    signature['minY'] = min_y
    signature['maxX'] = max_x
    signature['maxY'] = max_y
    signature['widthX'] = width_x
    signature['heightY'] = height_y
    signature['size'] = size
    signature['maxdX'] = max_dX
    signature['maxdY'] = max_dY

    # Convert to dataframe, transpose and return
    return signature.to_frame().T


# Generate dataframe from signature files
dataFrame = generate_dataframe(FILE_DIR, feature_extraction)

# Save dataframe as new csv file
dataFrame.to_csv(FILE_DIR + 'dataset.csv', index=False, header=True)
