import os

FILE_DIR = "data/Signatures-Mobile-Non-S8/"

FIRST_NAME = "Test"
LAST_NAME = "Test"

renameCount = 0
skipCount = 0
for filename in os.listdir(FILE_DIR):

    if filename.startswith("sig") and filename.endswith(".csv") and len(filename.split("-")) == 10:
        # File is a signature
        fileParams = filename.split("-")

        if fileParams[1] == "Unknown" and fileParams[2] == "Unknown":
            new_file_name = fileParams[0] + "-" + FIRST_NAME + "-" + LAST_NAME + "-" + fileParams[3] + "-" + fileParams[4] + "-" + fileParams[5] + "-" + fileParams[6] + "-" + fileParams[7] + "-" + fileParams[8] + "-" + fileParams[9]

            os.rename(FILE_DIR + filename, FILE_DIR + new_file_name)
            renameCount += 1
        else:
            skipCount += 1
    else:
        skipCount += 1

print("Completed operation")
print("Files renamed: ", renameCount)
print("Files skipped: ", skipCount)