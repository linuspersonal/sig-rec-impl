import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

FILE_DIR_MOBILE = "data/dataset-mobile/"
FILE_NAME_MOBILE = "dataset.csv"

OUTPUT_DIR_MOBILE = "plots/dataset-stats/mobile/"

if not os.path.exists(OUTPUT_DIR_MOBILE):
    os.makedirs(OUTPUT_DIR_MOBILE)


df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

df_mobile_genuine = df_mobile.loc[(df_mobile['genuine'] == 1)].reset_index(drop=True)
df_mobile_forgeries = df_mobile.loc[(df_mobile['genuine'] == 0)].reset_index(drop=True)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

print(df_mobile_genuine['age'].max())
count, bins = np.histogram(df_mobile_genuine['age'], bins = range(15, 90, 5))
count = count / 90
plt.hist(bins[:-1], bins, weights=count)
plt.xlabel("Age")
plt.ylabel("Number of Participants")
plt.title("Age Distribution of Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'age-distribution-mobile.pdf')
plt.show()      # This creates a new plot, so savefig has to come before

count, bins = np.histogram(df_mobile_genuine['dsize'], bins = np.arange(4.5, 7.5, 0.5))
count = count / 90
plt.hist(bins[:-1], bins, weights=count)
plt.xlabel("Screen Size (Inches)")
plt.ylabel("Number of Participants")
plt.title("Screen Size Distribution over Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'display-size-distribution-mobile.pdf')
plt.show()

ts_groups = df_mobile_genuine.groupby(['pID', 'session'])['ts'].sum() / 45  # Nr. of signatures
time_difference_seconds = (ts_groups.loc[:, 2] - ts_groups.loc[:, 1]) / 1000    # Ms to s
time_difference_days = time_difference_seconds / 86400  # s to days
time_difference_days = time_difference_days.round()

count, bins = np.histogram(time_difference_days)
plt.hist(bins[:-1], bins, weights=count)
plt.xlabel("Days between Sessions")
plt.ylabel("Number of Participants")
plt.title("Distribution of days between Sessions over Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'recording-session-delay-distribution.pdf')
plt.show()

devices = df_mobile_genuine['devmod'].value_counts()/90
plt.pie(devices, labels=devices.index, rotatelabels=True,
        autopct=lambda p: '{:.0f}'.format(p * sum(devices) / 100),
        pctdistance=0.8, startangle=340)     # 60 also works well
plt.title("Devices used")
plt.tight_layout()
plt.savefig(OUTPUT_DIR_MOBILE + 'used-devices-mobile.pdf', bbox_inches = 'tight')
plt.show()

touchsize = df_mobile_genuine['tsQ'].value_counts()/90
plt.pie(touchsize, labels=touchsize.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(touchsize) / 100),
        pctdistance=0.8, startangle=340)     # 60 also works well
plt.title("Touchsize Quality for Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'touchsize-quality.pdf', bbox_inches = 'tight')
plt.show()

sex = df_mobile_genuine['sex'].value_counts()/90
plt.pie(sex, labels=sex.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(sex) / 100))
plt.title("Participant Sex")
plt.savefig(OUTPUT_DIR_MOBILE + 'participant-sex-mobile.pdf', bbox_inches = 'tight')
plt.show()

hand = df_mobile_genuine['hand'].value_counts()/90
plt.pie(hand, labels=hand.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(hand) / 100))
plt.title("Right- and Left-Handed Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'signing-hand-mobile.pdf', bbox_inches = 'tight')
plt.show()

phrel = df_mobile_genuine['phrel'].value_counts()/90
plt.pie(phrel, labels=phrel.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(phrel) / 100))
plt.title("Release Dates of Participants Devices")
plt.savefig(OUTPUT_DIR_MOBILE + 'phone-release-dates-mobile.pdf', bbox_inches = 'tight')
plt.show()

lang = df_mobile_genuine['lang'].value_counts()/90
plt.pie(lang, labels=lang.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(lang) / 100))
plt.title("Native Language of Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'native-language-mobile.pdf', bbox_inches = 'tight')
plt.show()

country = df_mobile_genuine['country'].value_counts()/90
plt.pie(country, labels=country.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(country) / 100))
plt.title("Home Country of Participants")
plt.savefig(OUTPUT_DIR_MOBILE + 'home-country-participants-mobile.pdf', bbox_inches = 'tight')
plt.show()

# languages = df_touchpad['hand'].value_counts()

# df = df_mobile.loc[(df_mobile['lang'] == 'german') | (df_mobile['lang'] == 'German ')]

#ax = df_mobile_genuine['age'].plot.hist(bins = range(15, 90, 5), weights=)
#plt.show()

# a = df_mobile['devmod'].value_counts()/90
# b = df_touchpad['devmod'].value_counts()/45

# def add(a, b):
#    return a + b

# print(a.combine(b, add, fill_value = 0).sort_values(ascending=False))
