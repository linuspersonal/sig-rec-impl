import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

FILE_DIR_TOUCHPAD = "data/dataset-touchpad/"
FILE_NAME_TOUCHPAD = "dataset.csv"

OUTPUT_DIR_TOUCHPAD = "plots/dataset-stats/touchpad/"


if not os.path.exists(OUTPUT_DIR_TOUCHPAD):
    os.makedirs(OUTPUT_DIR_TOUCHPAD)


df_touchpad = pd.read_csv(FILE_DIR_TOUCHPAD + FILE_NAME_TOUCHPAD)

df_touchpad_genuine = df_touchpad.loc[(df_touchpad['genuine'] == 1)].reset_index(drop=True)
df_touchpad_forgeries = df_touchpad.loc[(df_touchpad['genuine'] == 0)].reset_index(drop=True)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

count, bins = np.histogram(df_touchpad_genuine['age'], bins = range(15, 90, 5))
count = count / 90
plt.hist(bins[:-1], bins, weights=count)
plt.xlabel("Age")
plt.ylabel("Number of Participants")
plt.title("Age Distribution of Participants")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'age-distribution-touchpad.pdf')
plt.show()      # This creates a new plot, so savefig has to come before

ts_groups = df_touchpad_genuine.groupby(['pID', 'session'])['ts'].sum() / 20  # Nr. of signatures
time_difference_seconds = (ts_groups.loc[:, 2] - ts_groups.loc[:, 1]) / 1000    # Ms to s
time_difference_days = time_difference_seconds / 86400  # s to days
time_difference_days = time_difference_days.round()

count, bins = np.histogram(time_difference_days)
plt.hist(bins[:-1], bins, weights=count)
plt.xlabel("Days between Sessions")
plt.ylabel("Number of Participants")
plt.title("Distribution of days between Sessions over Participants")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'recording-session-delay-distribution-touchpad.pdf')
plt.show()

sex = df_touchpad_genuine['sex'].value_counts() / 40
plt.pie(sex, labels=sex.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(sex) / 100))
plt.title("Participant Sex")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'participant-sex-touchpad.pdf', bbox_inches = 'tight')
plt.show()

hand = df_touchpad_genuine['hand'].value_counts() / 40
plt.pie(hand, labels=hand.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(hand) / 100))
plt.title("Right- and Left-Handed Participants")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'signing-hand-touchpad.pdf', bbox_inches = 'tight')
plt.show()

lang = df_touchpad_genuine['lang'].value_counts() / 40
plt.pie(lang, labels=lang.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(lang) / 100))
plt.title("Native Language of Participants")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'native-language-touchpad.pdf', bbox_inches = 'tight')
plt.show()

country = df_touchpad_genuine['country'].value_counts() / 40
plt.pie(country, labels=country.index,
        autopct=lambda p: '{:.0f}'.format(p * sum(country) / 100))
plt.title("Home Country of Participants")
plt.savefig(OUTPUT_DIR_TOUCHPAD + 'home-country-participants-touchpad.pdf', bbox_inches = 'tight')
plt.show()
