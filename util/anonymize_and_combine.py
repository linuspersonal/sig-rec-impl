import os
import shutil
import pandas as pd
import time
import datetime

MOBILE_SIGNATURES = "data/study-data-mobile/"
TOUCHPAD_SIGNATURES = "data/study-data-touchpad/"
MOBILE_FORGERIES = "data/forgeries-mobile/"
TOUCHPAD_FORGERIES = "data/forgeries-touchpad/"
MOBILE_OUTPUT = "data/dataset-mobile/"
TOUCHPAD_OUTPUT = "data/dataset-touchpad/"

QUESTIONNAIRE_FILE = "questionnaire.csv"
QUESTIONNAIRE_NAME_COL = "Bitte geben Sie Ihren vollen Namen an."

SIGNATURE_DIR = MOBILE_SIGNATURES
FORGERY_DIR = MOBILE_FORGERIES
OUTPUT_DIR = MOBILE_OUTPUT
FULL_CONSISTENCY = True     # Whether the participant numbers should be consistent across datasets


def get_subject_list(signature_dir: str, forgery_dir: str) -> [str]:
    signature_directories = [name for name in os.listdir(signature_dir)
                                    if os.path.isdir(os.path.join(signature_dir, name))]
    forgery_directories = [name for name in os.listdir(forgery_dir)
                                    if os.path.isdir(os.path.join(forgery_dir, name))]

    questionnaire_results = pd.read_csv(signature_dir + QUESTIONNAIRE_FILE)
    qr_list = questionnaire_results[QUESTIONNAIRE_NAME_COL].tolist()
    qr_list = [name.replace(" ", "-", 1).replace(" ", "").casefold() for name in qr_list]
    in_questionnaire = set(qr_list)

    in_signatures = set(signature_directories)
    in_forgeries = set(forgery_directories)
    in_forgeries_but_not_in_signatures = in_forgeries - in_signatures

    data_names = sorted(signature_directories) + sorted(list(in_forgeries_but_not_in_signatures))
    data_names = [name.casefold() for name in data_names]
    in_questionnaire_but_not_in_data = in_questionnaire - set(data_names)

    # All participants in the signature dataset first, then forgers that did not participate, then
    # questionnaire participants that are not in either dataset
    return data_names + sorted(list(in_questionnaire_but_not_in_data))


def get_fully_consistent_subject_list() -> [str]:
    mobile_signature_directories = [name for name in os.listdir(MOBILE_SIGNATURES)
                                    if os.path.isdir(os.path.join(MOBILE_SIGNATURES, name))]
    touchpad_signature_directories = [name for name in os.listdir(TOUCHPAD_SIGNATURES)
                                      if os.path.isdir(os.path.join(TOUCHPAD_SIGNATURES, name))]
    mobile_questionnaire_results = pd.read_csv(MOBILE_SIGNATURES + QUESTIONNAIRE_FILE)
    touchpad_questionnaire_results = pd.read_csv(TOUCHPAD_SIGNATURES + QUESTIONNAIRE_FILE)

    mqr_list = mobile_questionnaire_results[QUESTIONNAIRE_NAME_COL].tolist()
    tqr_list = touchpad_questionnaire_results[QUESTIONNAIRE_NAME_COL].tolist()
    mqr_list = [name.replace(" ", "-", 1).replace(" ", "").casefold() for name in mqr_list]
    tqr_list = [name.replace(" ", "-", 1).replace(" ", "").casefold() for name in tqr_list]
    in_questionnaire = set(mqr_list) | set(tqr_list)

    mobile_forgery_directories = [name for name in os.listdir(MOBILE_FORGERIES)
                                  if os.path.isdir(os.path.join(MOBILE_FORGERIES, name))]
    touchpad_forgery_directories = [name for name in os.listdir(TOUCHPAD_FORGERIES)
                                    if os.path.isdir(os.path.join(TOUCHPAD_FORGERIES, name))]

    in_signatures = set(mobile_signature_directories) | set(touchpad_signature_directories)
    in_forgeries = set(mobile_forgery_directories) | set(touchpad_forgery_directories)
    in_forgeries_but_not_in_signatures = in_forgeries - in_signatures

    data_names =  sorted(list(in_signatures)) + sorted(list(in_forgeries_but_not_in_signatures))
    data_names = [name.casefold() for name in data_names]
    in_questionnaire_but_not_in_data = in_questionnaire - set(data_names)

    return data_names + sorted(list(in_questionnaire_but_not_in_data))


def anonymize_and_combine(signature_dir: str, forgery_dir: str, output_dir: str, full_consistency: bool):

    subject_list = get_fully_consistent_subject_list() if full_consistency else get_subject_list(signature_dir, forgery_dir)
    subject_ids = range(0, len(subject_list))
    subject_dict = dict(zip(subject_list, subject_ids))

    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir)

    # Get all signature files in all subdirectories
    signature_files = list()
    for (dirpath, dirnames, filenames) in os.walk(signature_dir, topdown=True):
        signature_files += [os.path.join(dirpath.replace(signature_dir, ""), file)
                            for file in filenames if file.startswith('sig') and file.endswith('.csv')]

    for file in signature_files:
        file_path_params = file.split('/')
        file_params = file_path_params[2].split('-')

        folder_name = file_path_params[0]
        session_count = file_path_params[1]
        file_name = file_params[1] + "-" + file_params[2]

        if not folder_name.casefold() == file_name.casefold():
            print("WARNING: Signatures might be in wrong folder")
            print(folder_name.casefold())
            print(file_name.casefold())
            exit(1)

        current_id = subject_dict.get(folder_name.casefold())
        current_dir = str(current_id) + "/" + str(session_count) + "/"

        if not os.path.exists(output_dir + current_dir):
            os.makedirs(output_dir + current_dir)

        shutil.copy(signature_dir + file, output_dir + current_dir)

        os.rename(output_dir + current_dir + file_path_params[2],
                  output_dir + current_dir + file_path_params[2].replace(file_name, str(current_id)))

    exclude = {"leftover"}
    forgery_files = list()
    for (dirpath, dirnames, filenames) in os.walk(forgery_dir, topdown=True):
        dirnames[:] = [d for d in dirnames if d not in exclude]
        forgery_files += [os.path.join(dirpath.replace(forgery_dir, ""), file)
                            for file in filenames if file.startswith('sig') and file.endswith('.csv')]

    for file in forgery_files:
        file_path_params = file.split('/')
        file_params = file_path_params[1].split("-")

        forger_name = file_path_params[0]
        target_name = file_params[1] + "-" + file_params[2]

        if forger_name.casefold() == target_name.casefold():
            print("WARNING: Forger and target names are the same")
            print(forger_name.casefold)
            print(target_name.casefold)
            exit(1)

        if not file_params[4] == "false":
            print("WARNING: Signature is marked as genuine")
            print(forger_name.casefold)
            print(target_name.casefold)
            exit(1)

        forger_id = subject_dict.get(forger_name.casefold())
        target_id = subject_dict.get(target_name.casefold())

        if forger_id is None or target_id is None:
            print("WARNING: ID could not be found in subject list")
            print(forger_name.casefold())
            print(target_name.casefold())
            exit(1)

        current_dir = str(target_id) + "/" + "f" + str(forger_id) + "/"

        if not os.path.exists(output_dir + current_dir):
            os.makedirs(output_dir + current_dir)

        shutil.copy(forgery_dir + file, output_dir + current_dir)

        os.rename(output_dir + current_dir + file_path_params[1],
                  output_dir + current_dir + file_path_params[1].replace(target_name, str(target_id)))


def anonymize_questionnaire_results(signature_dir: str, forgery_dir: str, output_dir: str, full_consistency: bool):

    subject_list = get_fully_consistent_subject_list() if full_consistency else get_subject_list(signature_dir, forgery_dir)
    subject_ids = range(0, len(subject_list))
    subject_dict = dict(zip(subject_list, subject_ids))

    print(subject_dict)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    q_answers = pd.read_csv(signature_dir + QUESTIONNAIRE_FILE)
    q_answers[QUESTIONNAIRE_NAME_COL] = q_answers[QUESTIONNAIRE_NAME_COL]\
        .apply(lambda name: subject_dict.get(name.replace(" ", "-", 1).replace(" ", "").casefold()))

    # Timestamp on signatures is in UTC time, so we also want that here - ignore timezone string
    q_answers['Zeitstempel'] = q_answers['Zeitstempel']\
        .apply(lambda tstr: int(time.mktime(
            datetime.datetime.strptime(tstr, '%Y/%m/%d %I:%M:%S %p OEZ').timetuple()) * 1000))   # Millis

    q_answers.to_csv(output_dir + QUESTIONNAIRE_FILE, index=False, header=True)


a = pd.read_csv(SIGNATURE_DIR + QUESTIONNAIRE_FILE, encoding='utf-8')

print("test")

anonymize_questionnaire_results(SIGNATURE_DIR, FORGERY_DIR, OUTPUT_DIR, FULL_CONSISTENCY)

# anonymize_and_combine(SIGNATURE_DIR, FORGERY_DIR, OUTPUT_DIR, FULL_CONSISTENCY)
