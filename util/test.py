import numpy as np
import re

from ast import literal_eval

test = [[np.array([0])]*2]*3

test[0][0] = np.add(test[0][0], np.array([1]))

print(test)

test = [[np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])]]

# -------------

names = ["Nearest Neighbors",
         "Linear SVM",
         "RBF SVM",
         "Decision Tree",
         "Random Forest",
         "Neural Net",
         "AdaBoost",
         "Naive Bayes"
    ]

NR_GENUINE_TRAIN = [10, 20, 40, 70]

avg_rf_matrices = [[np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])]]
avg_sk_matrices = [[np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
        [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])]]

file = open("outputs/mobile/ml-global-all/subject-others-movement-only-error.txt", "r")
content = file.read()

relevant_content = content.split("Subject label:")
relevant_content.pop(0)

relevant_content[len(relevant_content)-1] = relevant_content[len(relevant_content)-1].split("OVERALL RESULTS:")[0]

for i in range(0, len(relevant_content)):
        models_string = relevant_content[i]

        models = models_string.split("\n\n")
        models.pop(0)

        for j in range(0, len(models)):
                model_results_string = models[j]

                model_results = model_results_string.split("]\n")

                model_results_rf = model_results[0]
                model_results_sk = model_results[1]
                model_results_rf = model_results_rf.replace("RF Matrices: [", "")
                model_results_sk = model_results_sk.replace("SK Matrices: [", "")

                rf_results = model_results_rf.split("), ")
                sk_results = model_results_sk.split("), ")

                for k in range(0, len(rf_results)):
                        current_np_array_rf = rf_results[k]
                        current_np_array_sk = sk_results[k]

                        current_np_array_rf = current_np_array_rf.replace("array(", "")
                        current_np_array_rf = current_np_array_rf.replace(", dtype=int64", "")
                        current_np_array_rf = current_np_array_rf.replace(",", "")
                        current_np_array_rf = current_np_array_rf.replace(")", "")

                        current_np_array_sk = current_np_array_sk.replace("array(", "")
                        current_np_array_sk = current_np_array_sk.replace(", dtype=int64", "")
                        current_np_array_sk = current_np_array_sk.replace(",", "")
                        current_np_array_sk = current_np_array_sk.replace(")", "")

                        current_np_array_rf = re.sub(r"([^[])\s+([^]])", r"\1, \2", current_np_array_rf)
                        current_np_array_sk = re.sub(r"([^[])\s+([^]])", r"\1, \2", current_np_array_sk)

                        result_rf = np.array(literal_eval(current_np_array_rf))
                        result_sk = np.array(literal_eval(current_np_array_sk))

                        avg_rf_matrices[j][k] = np.add(avg_rf_matrices[j][k], result_rf)
                        avg_sk_matrices[j][k] = np.add(avg_sk_matrices[j][k], result_sk)

print("OVERALL RESULTS:")
print()

for i in range(0, len(names)):
    print(names[i])
    print()

    for j in range(0, len(NR_GENUINE_TRAIN)):
        print("NR. of gen. Signatures:", NR_GENUINE_TRAIN[j])
        cur_avg_sk_matrix = np.divide(avg_sk_matrices[i][j], len(relevant_content))
        cur_avg_rf_matrix = np.divide(avg_rf_matrices[i][j], len(relevant_content))

        print("Random Forgeries: ")
        print(cur_avg_rf_matrix)
        tn, fp, fn, tp = cur_avg_rf_matrix.ravel()
        fpr = fp / (fp + tn)
        fnr = fn / (fn + tp)
        print("FPR:", fpr)      # False Acceptance Rate
        print("FNR:", fnr)      # False Rejection Rate
        print()

        print("Skilled Forgeries: ")
        print(cur_avg_sk_matrix)
        tn, fp, fn, tp = cur_avg_sk_matrix.ravel()
        fpr = fp / (fp + tn)
        fnr = fn / (fn + tp)
        print("FPR:", fpr)      # False Acceptance Rate
        print("FNR:", fnr)      # False Rejection Rate
        print()

    print("--------------------")
    print()

# test = "array([[20,  0],[17,  3]]), array([[20,  0],[ 6, 14]]), array([[20,  0],[ 4, 16]]), array([[20,  0],[ 4, 16]])"

# test2 = """array([[20,  0],
# [17,  3]], dtype=int64)"""
#
# test2 = test2.replace("array(", "")
# test2 = test2.replace(", dtype=int64)", "")
# test2 = test2.replace(",", "")
#
# test2 = re.sub(r"([^[])\s+([^]])", r"\1, \2", test2)
# result = np.array(literal_eval(test2))