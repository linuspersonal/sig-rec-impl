import numpy as np
import pandas as pd


def resample_fixed(df: pd.DataFrame, n_new: int) -> pd.DataFrame:
    """
    Resamples a signature to a fixed size. Brings every signature to the same
    length by interpolating the values accordingly. This stretches/crunches the
    signature if every value is assumed to be after the same time interval.
    :param df: The Dataframe to resample
    :param n_new: The new number of rows in the dataframe
    :return: The resampled dataframe with the new number of rows
    """

    n_old, m = df.values.shape
    mat_old = df.values
    mat_new = np.zeros((n_new, m))
    x_old = np.linspace(df.index.min(), df.index.max(), n_old)
    x_new = np.linspace(df.index.min(), df.index.max(), n_new)

    for j in range(m):
        y_old = mat_old[:, j]
        y_new = np.interp(x_new, x_old, y_old)
        mat_new[:, j] = y_new

    return pd.DataFrame(mat_new, index=range(0, n_new), columns=df.columns)


# df = pd.DataFrame({"A": [1, 2, 3, 4, 5, 6, 7, 8, 20, 100, 40, 20],})
# df_new = resample_fixed(df, 7)
