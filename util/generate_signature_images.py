import shutil

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

FILE_DIR = "data/study-data-mobile/"
IMAGE_DIR = "plots/new/study-data-mobile-pdf/"

INCLUDE_BOTH_SESSIONS = True
SAVE_AS_PDF = True


def draw_signatures(directory: str):
    """
    Generates a dataframe from all signatures in the specified folder.
    :param directory: The directory with the signatures to include in the dataframe.
    :param callback: A function that takes a signature as dataframe and the file parameters as
    arguments and returns it as a dataframe with one row with all extracted features.
    :return: Returns a dataframe with one row per signature.
    """

    if os.path.exists(IMAGE_DIR):
        shutil.rmtree(IMAGE_DIR)

    os.makedirs(IMAGE_DIR)

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    # Get all signature files in all subdirectories
    exclude = {"leftover"}
    signature_files = list()
    for (dirpath, dirnames, filenames) in os.walk(directory, topdown=True):
        dirnames[:] = [d for d in dirnames if d not in exclude]
        signature_files += [os.path.join(dirpath.replace(directory, ""), file)
                            for file in filenames if file.startswith('sig') and file.endswith('.csv')]

    df = None
    included_names = []
    for file in signature_files:

        session = -1
        file_params = []
        file_path_params = file.split('/')
        if len(file_path_params) == 2:
            # Forgeries
            session = 1
            file_params = file_path_params[1].split('-')
        elif len(file_path_params) == 3:
            if file_path_params[1].startswith('f'):
                session = int(file_path_params[1][1:]) + 2
            else:
                session = int(file_path_params[1])
            file_params = file_path_params[2].split('-')
        else:
            print("Unknown file path params: Exiting")
            exit(1)

        if (session == 1 or INCLUDE_BOTH_SESSIONS) and (session > 0) and session < 3:

            # Load the file
            signature = pd.read_csv(directory + file)

            screen_width_px = int(file_params[5])
            screen_height_px = int(file_params[6])

            signature.plot(x='x', y='y', kind='line')
            plt.axis([0, screen_width_px, 0, screen_height_px])
            plt.tick_params(axis='x', which='both', bottom=False, top=False)
            plt.tick_params(axis='y', which='both', left=False, right=False)
            plt.xticks(np.arange(2), "")
            plt.yticks(np.arange(2), "")
            plt.xlabel("")
            plt.ylabel("")
            # plt.xlabel("Screen X")
            # plt.ylabel("Screen Y")
            plt.legend('',frameon=False)
            # plt.legend(['Signature'])
            plt.gca().set_aspect('equal')
            plt.gca().invert_yaxis()
            # plt.rcParams.update({'font.size': 18})

            if SAVE_AS_PDF:
                plt.savefig(IMAGE_DIR + file_params[1] + "-" + file_params[2] + "-" + file_params[3] + ".pdf")
            else:
                plt.savefig(IMAGE_DIR + file_params[1] + "-" + file_params[2] + "-" + file_params[3] + ".png")

            plt.close()

            #fig, axes = plt.subplots(nrows=3, ncols=2)
            #signature.plot(ax=axes[0, 0], x='timestamp', y='gyroX', kind='line')
            #signature.plot(ax=axes[0, 1], x='timestamp', y='gyroY', kind='line')
            #signature.plot(ax=axes[1, 0], x='timestamp', y='gyroZ', kind='line')
            #signature.plot(ax=axes[1, 1], x='gyroX', y='gyroY', kind='scatter')
            #signature.plot(ax=axes[2, 0], x='gyroX', y='gyroZ', kind='scatter')
            #signature.plot(ax=axes[2, 1], x='gyroY', y='gyroZ', kind='scatter')
            #plt.savefig(IMAGE_DIR + file_params[1] + "-" + file_params[2] + "-" + file_params[3] + ".png")
            #plt.close()

            #signature.plot(x='timestamp', y='gravZ', kind='line')
            #plt.savefig(IMAGE_DIR + file_params[1] + "-" + file_params[2] + "-" + file_params[3] + ".png")
            #plt.close()


print("Running")
draw_signatures(FILE_DIR)
