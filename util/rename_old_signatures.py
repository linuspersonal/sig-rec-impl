import os

FILE_DIR = "data/Signatures-Mobile/"

renameCount = 0
skipCount = 0
for filename in os.listdir(FILE_DIR):

    if filename.startswith("sig") and filename.endswith(".csv") and len(filename.split("-")) == 5:
        new_file_name = filename.replace(".csv", "") + "-2175-1080-422-424-SMG950F.csv"

        os.rename(FILE_DIR + filename, FILE_DIR + new_file_name)
        renameCount += 1
    else:
        skipCount += 1

print("Completed operation")
print("Files renamed: ", renameCount)
print("Files skipped: ", skipCount)