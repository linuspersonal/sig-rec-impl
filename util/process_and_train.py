import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler


def print_grid_search_results(grid_search_model, name: str):
    """
    Prints the grid search results in a readable way.
    :param grid_search_model: The grid search results
    :param name: The name of the model to print
    """
    print(name, "| Best parameter set:")
    print()
    print(grid_search_model.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    means = grid_search_model.cv_results_['mean_test_score']
    stds = grid_search_model.cv_results_['std_test_score']
    for mean, std, pars in zip(means, stds, grid_search_model.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, pars))
    print()


def combine_label_and_genuine(df: pd.DataFrame, label_column: str, genuine_column: str) -> pd.DataFrame:
    """
    Combines the label and genuine/forgery indicator, so that genuine signatures
    and forgeries of that signature have a different label.
    Results in a dataframe where each persons genuine signatures and the forged signatures
    of each persons genuine signature have a unique label.
    :param df: The dataframe to operate on
    :param label_column: The name of the column containing the labels
    :param genuine_column: The name of the column containing the genuine indicator (as 0/1)
    :return: A dataframe with the combined label
    """
    # new_labels = df[label_column] + df[genuine_column].apply(lambda genuine: class_nr if genuine == 0 else 0)
    class_nr = df[label_column].max() + 1 # df[label_column].nunique()
    result = df.copy(deep=True)     # We do not want to modify the original dataframe
    result.loc[df[genuine_column] == 0, [label_column]] += class_nr
    return result


def normalize_colwise(df: pd.DataFrame, exclude: [str] = ('label')):
    """
    Normalizes all but the specified values column-wise using
    the numpy MinMaxScaler().
    :param df: The dataframe to normalize
    :param exclude: Columns to exclude from normalization
    :return: The normalized dataframe
    """
    excluded_df = df[exclude]
    df = df.drop(exclude, axis=1)

    numpy_arr = df.values  # returns a numpy array
    scaler = MinMaxScaler() # StandardScaler()
    numpy_arr = scaler.fit_transform(numpy_arr)
    df = pd.DataFrame(numpy_arr, columns=df.columns)
    df[exclude] = excluded_df
    return df
