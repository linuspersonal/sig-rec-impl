from itertools import chain

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import csv

from scipy.spatial import distance
from statistics import mean, stdev

from sklearn.metrics import balanced_accuracy_score, confusion_matrix, roc_curve

from util.process_and_train import normalize_colwise, combine_label_and_genuine

WINDOW_SIZE = 10


def multi_dim_dtw(s: tuple, t: tuple, window: int):
    """
    Returns a DTW matrix for the two given time series.
    The distance between the two series is calculated as
    the euclidian distance between the values in each tuple.
    :param s: The first time series as list of tuples
    :param t: The second time series as list of tuples
    :param window: The window within which to perform the dtw.
    :return: The dtw matrix, where the entry most to the bottom right represents the score.
    """

    n, m = len(s), len(t)
    w = np.max([window, abs(n - m)])
    dtw_matrix = np.zeros((n + 1, m + 1))

    for i in range(n + 1):
        for j in range(m + 1):
            dtw_matrix[i, j] = np.inf
    dtw_matrix[0, 0] = 0

    for i in range(1, n + 1):
        for j in range(np.max([1, i - w]), np.min([m, i + w]) + 1):
            dtw_matrix[i, j] = 0

    for i in range(1, n + 1):
        for j in range(np.max([1, i - w]), np.min([m, i + w]) + 1):
            # cost = abs(s[i-1] - t[j-1]) // Normal absolute cost
            cost = distance.euclidean(s[i - 1], t[j - 1])
            # take last min from a square box
            last_min = np.min([dtw_matrix[i - 1, j],
                               dtw_matrix[i, j - 1],
                               dtw_matrix[i - 1, j - 1]])
            dtw_matrix[i, j] = cost + last_min
    return dtw_matrix


def get_row_tuple(row: pd.Series, features: [str]) -> tuple:
    """
    Converts the specified time series features into a set of tuples such that all feature
    values at each (same) time point are together. Tuples can be used by dtw function.
    :param row: The row to extract the tuple set from.
    :param features: The features that should be used in each tuple.
    :return: The set of tuples.
    """
    row_features = [None] * len(features)
    for i in range(len(features)):
        row_features[i] = row.loc[row.index.str.startswith(features[i] + '_')]

    return tuple(zip(*row_features))  # The * unpacks the list, so the function receives the values element-wise


def intra_class_dtw(df: pd.DataFrame, class_label: int or None, features: [str]) -> [float]:
    """
    Calculate intra-class scores of all permutations of signatures of a class.
    Different orders of the same signatures are considered to be the same.
    :param df: The dataframe with the signatures. Can contain other classes.
    :param class_label: The class label of the signatures to calculate the score for.
    :param features: The features to include in the distance measure.
    :return: The list of dtw scores, in order of computation.
    """

    current_set = df
    if class_label is not None:
        current_set = df.loc[df['label'] == class_label].reset_index(drop=True)

    count = 0
    intra_class_scores = [None] * int(current_set.shape[0] * (current_set.shape[0] - 1) / 2)
    for i in range(0, current_set.shape[0] - 1):

        current_row_tuple = get_row_tuple(current_set.loc[i, :], features)

        for j in range(i + 1, current_set.shape[0]):
            print("Class", class_label, "| Iteration", count, "|", i, "x", j)

            comparison_row_tuple = get_row_tuple(current_set.loc[j, :], features)

            dtw_matrix = multi_dim_dtw(current_row_tuple, comparison_row_tuple, WINDOW_SIZE)
            intra_class_scores[count] = dtw_matrix[dtw_matrix.shape[0] - 1, dtw_matrix.shape[1] - 1]
            count += 1

    return intra_class_scores


def inter_class_dtw(df: pd.DataFrame, class_label: int, features: [str]) -> [float]:
    """
    Calculate inter-class scores for each signature of the class with all
    signatures not of the class.
    :param df: The dataframe with the signatures
    :param class_label: The label of the class under investigation
    :param features: The features to include in the distance measure
    :return: The list of dtw scores, in order of computation
    """
    current_set = df.loc[df['label'] == class_label].reset_index(drop=True)
    other_set = df.loc[df['label'] != class_label].reset_index(drop=True)

    count = 0
    inter_class_scores = [None] * int(current_set.shape[0] * other_set.shape[0])
    for i in range(0, current_set.shape[0]):

        current_row_tuple = get_row_tuple(current_set.loc[i, :], features)

        for j in range(0, other_set.shape[0]):
            print("Class", class_label, "| Iteration", count, "|", i, "x", j)

            other_row_tuple = get_row_tuple(other_set.loc[j, :], features)

            dtw_matrix = multi_dim_dtw(current_row_tuple, other_row_tuple, WINDOW_SIZE)
            inter_class_scores[count] = dtw_matrix[dtw_matrix.shape[0] - 1, dtw_matrix.shape[1] - 1]
            count += 1

    return inter_class_scores


def plot_class_results(intra_class_results: [float], inter_class_results: [float], class_label: int):
    """
    Plots the inter- and intra-class results as two boxplots in one plot.
    :param intra_class_results: The intra-class-results to plot.
    :param inter_class_results: The inter-class-results to plot.
    :param class_label: The class label of the plotted class for the plot title.
    """
    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title('Class ' + str(class_label) + ": Inter- and Intra-Class distances")
    plt.boxplot([intra_class_results, inter_class_results], labels=["Intra-Class", "Inter-Class"], showfliers=True)
    plt.show()


def plot_class_forgery_results(intra_class_results: [float], forgery_results: [float], class_label: int):
    """
    Plots the intra-class and class-forgery results as two boxplots in one plot.
    :param intra_class_results: The intra-class-results to plot.
    :param forgery_results: The class-forgery-results to plot.
    :param class_label: The class label of the plotted class for the plot title.
    """
    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title('Class ' + str(class_label) + ": Intra-Class and Class-Forgery distances")
    plt.boxplot([intra_class_results, forgery_results], labels=["Intra-Class", "Class-Forgery"], showfliers=True)
    plt.show()


def plot_combined_results(intra_class_results: [float], inter_class_results: [float], forgery_results: [float],
                          class_label: int):
    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title('Class ' + str(class_label) + ": Intra-Class, Inter-Class and Class-Forgery distances")
    plt.boxplot([intra_class_results, inter_class_results, forgery_results],
                labels=["Intra-Class", "Inter-Class", "Class-Forgery"], showfliers=True)
    plt.show()


def plot_mean_results(intra_class_results: [[float]], inter_class_results: [[float]]):
    """
    Plot distribution of inter- and intra-class mean values between classes. Calculates
    the means for the inter- and intra-class results of each class and displays their
    distribution in a boxplot.
    :param intra_class_results: The set of intra-class results to plot.
    :param inter_class_results: The set of inter-class results to plot.
    """
    intra_class_means = [None] * len(intra_class_results)
    inter_class_means = [None] * len(inter_class_results)
    for i in range(0, len(intra_class_results)):
        intra_class_means[i] = mean(intra_class_results[i])
        inter_class_means[i] = mean(inter_class_results[i])

    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title("Inter- and Intra-Class means distribution")
    plt.boxplot([intra_class_means, inter_class_means], labels=["Intra-Class", "Inter-Class"], showfliers=True)
    plt.show()


def plot_overview(intra_class_results: [[float]], inter_class_results: [[float]]):
    """
    Results in a plot similar to plot_mean_results. However, this includes ALL results
    directly in the two resulting boxplots, without calculating the means first.
    :param intra_class_results: The set of intra-class results to plot
    :param inter_class_results: The set of inter-class results to plot
    """
    intra_class_vals = list(chain.from_iterable(intra_class_results))
    inter_class_vals = list(chain.from_iterable(inter_class_results))

    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title("Overall Inter- and Intra-Class/CF result distribution")
    plt.boxplot([intra_class_vals, inter_class_vals], labels=["Intra-Class", "Inter-Class/CF"], showfliers=True)
    plt.show()


def plot_combined_overview(intra_class_results: [[float]], inter_class_results: [[float]], forgery_results: [[float]]):
    intra_class_vals = list(chain.from_iterable(intra_class_results))
    inter_class_vals = list(chain.from_iterable(inter_class_results))
    forgery_vals = list(chain.from_iterable(forgery_results))

    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_title("Overall Intra, Inter- and Class-Forgery-results")
    plt.boxplot([intra_class_vals, inter_class_vals, forgery_vals],
                labels=["Intra-Class", "Inter-Class", "Class-Forgery"], showfliers=True)
    plt.show()


def plot_results(intra_class_results: [[float]], inter_class_results: [[float]]):
    """
    Plots the boxplots for each class in the set and the overall means boxplot.
    :param intra_class_results: The set of intra-class results to plot.
    :param inter_class_results: The set of inter-class results to plot.
    """
    for i in range(0, len(intra_class_results)):
        plot_class_results(intra_class_results[i], inter_class_results[i], i)

    plot_overview(intra_class_results, inter_class_results)


def save_result(result: [[float]], filename: str, path="data/"):
    """
    Saves the result set of a dtw computation as .csv file. Creates the directory
    if it does not exist and will overwrite existing files with the same name.
    :param result: The result to write to the .csv file
    :param filename: The name of the file to write to, ending with .csv
    :param path: The path where to save the file, ending with "/"
    """
    if not os.path.exists(path):
        os.makedirs(path)

    with open(path + filename, "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(result)


def read_result(filename: str, path="data/") -> [[float]]:
    with open(path + filename, newline="") as f:
        reader = csv.reader(f)
        data = list(reader)

    return [[float(j) for j in i] for i in data]  # Conversion to float array


def compute_dtw(df: pd.DataFrame, features: [str], plot=False, save=False):
    """
    Computes both the inter- and intra-class dtws for each class in the dataframe.
    Can optionally immediately plot and save the intermediary results.
    It doesn't matter if the dtw is calculated before or after stretching to same length.
    It is just a linear transformation DTW can deal with.
    :param df: The dataframe to compute the dtw for
    :param features: The features to include in the distance calculation
    :param plot: Whether or not to plot the intermediary results
    :param save: Whether or not to save the intermediary results to csv
    :return: Returns the sets of intra- and inter-class dtw results
    """
    number_of_classes = df['label'].max() + 1  # Classes start with 0
    intra_class_results = [[]] * number_of_classes
    inter_class_results = [[]] * number_of_classes

    for class_label in range(0, number_of_classes):

        intra_class_results[class_label] = intra_class_dtw(df, class_label, features)

        inter_class_results[class_label] = inter_class_dtw(df, class_label, features)

        if plot:
            plot_class_results(intra_class_results[class_label], inter_class_results[class_label], class_label)

        if save:
            save_result(intra_class_results, "intra_class_results.csv", "data/dtw/")
            save_result(inter_class_results, "inter_class_results.csv", "data/dtw/")

    if plot:
        plot_overview(intra_class_results, inter_class_results)

    return intra_class_results, inter_class_results


def compute_class_forgery_dtw(df: pd.DataFrame, features: [str], plot=False, save=False):
    """
    Computes the DTW between a signature and its forgeries. Dataframe is required to
    have labels for genuine/forgery identification
    :param df: The dataframe to compute the DTW for
    :param features: The features to use for the DTW
    :param plot: Whether or not to plot the results for each iteration
    :param save: Whether or not to save the results for each iteration
    """

    number_of_classes = df['label'].max() + 1  # Classes start with 0
    intra_class_results = [[]] * number_of_classes
    class_forgery_results = [[]] * number_of_classes

    df = combine_label_and_genuine(df, 'label', 'genuine')

    for class_label in range(0, number_of_classes):

        # We only want to compute the intra-class dtw between current class and its forgeries
        current_df = df.query('label == {0} | label == {1}'.format(class_label, class_label + number_of_classes))

        intra_class_results[class_label] = intra_class_dtw(current_df, class_label, features)

        class_forgery_results[class_label] = inter_class_dtw(current_df, class_label, features)

        if plot:
            plot_class_forgery_results(intra_class_results[class_label], class_forgery_results[class_label],
                                       class_label)

        if save:
            save_result(intra_class_results, "intra_class_results_cf.csv", "data/dtw/")
            save_result(class_forgery_results, "class_forgery_results.csv", "data/dtw/")

    if plot:
        plot_overview(intra_class_results, class_forgery_results)

    return intra_class_results, class_forgery_results


def compute_combined_dtw(df: pd.DataFrame, features: [str], plot=False, save=False):
    number_of_classes = df['label'].max() + 1  # Classes start with 0
    intra_class_results = [[]] * number_of_classes
    inter_class_results = [[]] * number_of_classes
    class_forgery_results = [[]] * number_of_classes

    df = combine_label_and_genuine(df, 'label', 'genuine')

    for class_label in range(0, number_of_classes):

        # We want the "normal" dtws on the normal dataset, without forgeries
        current_df = df.query('label < {0}'.format(number_of_classes))
        # We only want to compute the class-forgery dtw between current class and its forgeries
        class_forgery_df = df.query('label == {0} | label == {1}'.format(class_label, class_label + number_of_classes))

        intra_class_results[class_label] = intra_class_dtw(current_df, class_label, features)

        inter_class_results[class_label] = inter_class_dtw(current_df, class_label, features)

        class_forgery_results[class_label] = inter_class_dtw(class_forgery_df, class_label, features)

        if plot:
            plot_combined_results(intra_class_results[class_label], inter_class_results[class_label],
                                  class_forgery_results[class_label], class_label)

        if save:
            save_result(intra_class_results, "intra_class_results_cf.csv", "data/dtw/")
            save_result(inter_class_results, "inter_class_results.csv", "data/dtw/")
            save_result(class_forgery_results, "class_forgery_results.csv", "data/dtw/")

    if plot:
        plot_combined_overview(intra_class_results, inter_class_results, class_forgery_results)

    return intra_class_results, inter_class_results, class_forgery_results


def dtw_classification(train: pd.DataFrame, test: pd.DataFrame, features: [str]):
    intra_class_distances = intra_class_dtw(train, None, features)
    avg_icd = mean(intra_class_distances)
    stdev_icd = stdev(intra_class_distances)

    threshold = avg_icd + stdev_icd * 1.5

    distances = [None] * test.shape[0]
    for i in range(0, test.shape[0]):

        current_row_tuple = get_row_tuple(test.loc[i, :], features)

        scores = [None] * train.shape[0]
        for j in range(0, train.shape[0]):
            comparison_row_tuple = get_row_tuple(train.loc[j, :], features)

            dtw_matrix = multi_dim_dtw(current_row_tuple, comparison_row_tuple, WINDOW_SIZE)
            scores[j] = dtw_matrix[dtw_matrix.shape[0] - 1, dtw_matrix.shape[1] - 1]

        distances[i] = mean(scores)
        # distances[i] = 1 if mean_score <= threshold else -1
        # print("Computed", i, "'s mean distance to template:", distances[i])

    return distances, intra_class_distances


def calculate_eer(y_true, distances):
    fpr, tpr, thresholds = roc_curve(y_true, distances, pos_label=1)
    fnr = 1 - tpr

    eer = fpr[np.nanargmin(np.absolute((fnr - fpr)))]
    eer_check = fnr[np.nanargmin(np.absolute((fnr - fpr)))]
    eer_threshold = thresholds[np.nanargmin(np.absolute((fnr - fpr)))]
    return eer, eer_check, eer_threshold


def predict(model, distances):
    avg_icd = mean(model)
    stdev_icd = stdev(model)
    threshold = avg_icd + stdev_icd * 1.5

    preds = [1 if dist <= threshold else -1 for dist in distances]
    return preds


def select_data(df: pd.DataFrame, label, stances: [str],
                samples_first: [int], samples_second: [int], random: bool) -> pd.DataFrame:
    df = df.loc[(df['label'] == label)] if label is not None else df

    data = None
    for stance, nr_first, nr_second in zip(stances, samples_first, samples_second):

        first = df.loc[(df['pos'].str.match(stance)) & (df['session'] == 1)]
        first = first.sample(nr_first) if random else first.nsmallest(nr_first, 'ts')

        second = df.loc[(df['pos'].str.match(stance)) & (df['session'] == 2)]
        second = second.sample(nr_second) if random else second.nsmallest(nr_second, 'ts')

        if data is None:
            data = first.append(second)
        else:
            data = data.append(first.append(second))

    return data


def dtw_results_to_string(df: pd.DataFrame, results: ([], [], [], [], [])) -> str:
    eers, eer_checks, eer_thresholds, test_distances, train_distances = results

    classes = np.sort(df['label'].unique())
    result_pos = 0
    result_string = ""
    for cur_label in classes:
        # Get the participant id for the current label. Calculating mean as additional check, because if
        # any one participant id is not the same, the mean will be off as well
        participant_id = df.loc[df['label'] == cur_label]['pID'].mean()

        result_string += f"Label: {cur_label}; Participant ID: {participant_id}\n"
        result_string += f"EER: {eers[result_pos]},    Check Value: {eer_checks[result_pos]}\n"
        result_string += f"EER Threshold: {eer_thresholds[result_pos]}\n"
        result_string += f"Avg. Intra-Class Dist: {mean(train_distances[result_pos])}, " \
                  f"StDev: {stdev(train_distances[result_pos])}\n"
        result_string += f"Avg. Inter-Class Dist: {mean(test_distances[result_pos])}, " \
                  f"StDev: {stdev(test_distances[result_pos])}\n"
        result_string += "\n"

        result_pos += 1

    result_string += "Overall Result:\n"
    result_string += f"Mean EER: {mean(eers)}, StDev: {stdev(eers)}"
    result_string += f"Mean Check Value: {mean(eer_checks)}, StDev: {stdev(eer_checks)}"
    return result_string


def write_dtw_output(folder: str, file: str, data: str, print_filename = False):
    if not os.path.exists(folder):
        os.makedirs(folder)

    output_file = open(folder + file, "w")
    output_file.write(data)
    output_file.close()

    if print_filename:
        print("Successfully written results to: " + file)


def classify_with_dtw(df: pd.DataFrame,
                      features: [str],
                      stance_train: [str],
                      stance_test: [str],
                      samples_train_first: [int],
                      samples_test_first: [int],
                      random_train: bool,
                      skilled_forgeries: bool,
                      samples_train_second=None,
                      samples_test_second=None) -> ([], [], [], [], []):
    classes = np.sort(df['label'].unique())
    forgery_offset = df['label'].max() + 1
    df = combine_label_and_genuine(df, 'label', 'genuine')

    # If nothing is specified, use same sample distribution for second session as for first session
    samples_train_second = samples_train_first if samples_train_second is None else samples_train_second
    samples_test_second = samples_test_first if samples_test_second is None else samples_test_second

    eers, eer_checks, eer_thresholds, test_distances, train_distances = [], [], [], [], []
    for cur_label in classes:
        print("Label", cur_label)

        # Select training data
        train = select_data(df, cur_label, stance_train, samples_train_first, samples_train_second, random_train)

        # Remove data already selected for training, then select testing data
        other = df.drop(index=train.index)

        # Select positive test data
        positive = select_data(other, cur_label, stance_test, samples_test_first, samples_test_second, random=True)

        # Select negative test data
        nr_negative = sum(samples_test_first) + sum(samples_test_second)
        if skilled_forgeries:
            # Select as many skilled forgeries as positive samples
            negative = other.loc[other['label'] == (cur_label + forgery_offset)].sample(nr_negative)
        else:
            # Randomly select random forgeries, with the same distribution of stance and session as in positive samples
            negative = other.loc[(other['label'] != cur_label) & (other['genuine'] == 1)]
            negative = select_data(negative, None, stance_test, samples_test_first, samples_test_second, random=True)

        test = positive.append(negative)

        # Reset indices of train and test datasets for cleanness
        test = test.reset_index(drop=True)
        train = train.reset_index(drop=True)

        # Label test data as genuine or forgery instead of with participant numbers
        test['label'] = test['label'].apply(lambda label: 1 if label == cur_label else -1)

        # Classify using Multi-Dimensional DTW and calculate results
        distances, model_distances = dtw_classification(train, test, features)
        eer, eer_check, eer_threshold = calculate_eer(test['label'], distances)

        # Store results
        eers.append(eer)
        eer_checks.append(eer_check)
        eer_thresholds.append(eer_threshold)
        test_distances.append(distances)
        train_distances.append(model_distances)

    return eers, eer_checks, eer_thresholds, test_distances, train_distances


FILE_DIR_MOBILE = "data/dataset-mobile/"
FILE_DIR_TOUCHPAD = "data/dataset-touchpad/"
FILE_NAME_MOBILE = "dataset.csv"
FILE_NAME_TOUCHPAD = "dataset.csv"

OUTPUT_DIR_MOBILE = "outputs/mobile/"
OUTPUT_DIR_TOUCHPAD = "outputs/touchpad/"

FEATURES_XY = [
    'x', 'y', 'fc',
    'dX', 'dY',
    'pta', 'dpta', 'pvm', 'dpvm', 'pvmr5',
    'lcr', 'dlcr', 'tam', 'dtam',
    'ddX', 'ddY', 'acs', 'dacs', 'sinacs', 'cosacs',
    'ltwr5', 'ltwr7'
]

FEATURES_ACC = ['accX', 'accY', 'accZ', 'accMag']
FEATURES_GRAV = ['gravX', 'gravY', 'gravZ'] # 'gravMag', - Leave out because always 9.81m/s²
FEATURES_GYRO = ['gyroX', 'gyroY', 'gyroZ', 'gyroMag']
FEATURE_TOUCHSIZE = ['touchsize']

FEATURES_MOVEMENT = FEATURES_ACC + FEATURES_GRAV + FEATURES_GYRO

FEATURES_COMBINED = FEATURES_XY + FEATURES_MOVEMENT

FEATURES_XY_TS = FEATURES_XY + FEATURE_TOUCHSIZE

FEATURES_XY_ACC = FEATURES_XY + FEATURES_ACC

FEATURES_XY_GRAV = FEATURES_XY + FEATURES_GRAV

FEATURES_XY_GYRO = FEATURES_XY + FEATURES_GYRO

FEATURES_COMBINED_TS = FEATURES_COMBINED + FEATURE_TOUCHSIZE
