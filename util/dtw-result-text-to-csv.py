import os
import re
import statistics

CUR_DIR = "outputs/mobile/dtw-scenario-4-xy-ind/random/"

results = []

for root, dirs, files in os.walk(CUR_DIR):
    for file in files:
        if file.endswith(".txt"):
            print(os.path.join(root, file))

            file = open(os.path.join(root, file), "r")
            content = file.read()
            subject_strings = content.split("\n\n")
            subject_strings.pop()

            eers = []

            for subject in subject_strings:
                result_strings = re.split(";|\n|,", subject)

                label = int(result_strings[0].split(":")[1])
                pID = int(float(result_strings[1].split(":")[1]))
                eer = float(result_strings[2].split(":")[1])
                eer_check = float(result_strings[3].split(":")[1])

                eers.append(eer)

            print(round(1-statistics.mean(eers), 4))
            print(round(statistics.stdev(eers), 4))
            results.append(eers)


