def compute_mean_and_stdev(participant_answers: [int], answer_numbers: [int] = None):

    if answer_numbers is None or len(answer_numbers) != len(participant_answers):
        answer_numbers = list(range(1, len(participant_answers)+1))

    mean = 0
    for i in range(0, len(participant_answers)):
        mean += participant_answers[i] * answer_numbers[i]

    mean /= sum(participant_answers)

    stdev = 0
    for i in range(0, len(participant_answers)):
        stdev += (answer_numbers[i] - mean) ** 2 * participant_answers[i]

    stdev /= sum(participant_answers)
    stdev = stdev ** (1/2.0)

    print("Mean:", mean)
    print("Stdev:", stdev)


compute_mean_and_stdev([20, 9, 4, 3, 9])
