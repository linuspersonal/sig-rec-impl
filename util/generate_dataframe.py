import pandas as pd
import os

# Needed for speed conversion
INCH_IN_MM = 25.4

# Whether to use the writer ID from the dataset or to generate a new one
GENERATE_NEW_ID = True


SIGNATURE_TAG = 0
PARTICIPANT_ID = 1
TIMESTAMP = 2
AUTHENTICITY = 3
SCREEN_WIDTH = 4
SCREEN_HEIGHT = 5
WIDTH_DPI = 6
HEIGHT_DPI = 7
DEVICE_MODEL = 8
RECORDING_POSITION = 9
SEX = 10
AGE = 11
COUNTRY = 12
LANGUANGE = 13
WRITING_HAND = 14

# (Screen size, Device Release Date, Touchsize Quality)
# Touchsize Quality:
# 0: Does not report or only reports one constant value
# 1: Reports very few, very constant values, very likely incorrect
# 2: Reports some different values, but sometimes suspiciously constant, correctness unclear
# 3: Reports different, changing values that are likely usable
DEVICE_IDENTIFIERS = {
    'SMG950F': (5.8, 2017, 3),
    'SMG930F': (5.1, 2016, 3),
    'ANELX1': (5.84, 2018, 0),
    'SMA520F': (5.2, 2017, 2),
    'SMA405FN': (5.9, 2019, 1),
    'ONEPLUS A6013': (6.41, 2018, 3),
    'FIGLX1': (5.65, 2017, 0),
    'WASLX1A': (5.2, 2017, 0),
    'SMG975F': (6.4, 2019, 3),
    'H8314': (5.0, 2018, 0),
    'Nokia 7 plus': (6.0, 2018, 2),
    'BV9800Pro': (6.3, 2019, 1),
    'POTLX1': (6.21, 2018, 3),
    'F5121': (5.0, 2016, 0),
    'Redmi Note 8': (6.3, 2019, 1),
    'MARLX1A': (6.15, 2019, 0),
    'SMG920F': (5.1, 2015, 3),
    'SMM205FN': (6.3, 2019, 2),
    'SMG960F': (5.8, 2018, 3),
    'ONEPLUS A3003': (5.5, 2016, 3),
    'HUAWEI CANL11': (5.0, 2016, 3),
    'COLL29': (5.84, 2018, 0),
    'GM1913': (6.67, 2019, 0),
    'SMG973F': (6.1, 2019, 3),
    'Mi A3': (6.09, 2019, 1),
    'SMA320FL': (4.7, 2017, 0),
    'ELEL29': (6.1, 2019, 3),
    'ONEPLUS A5010': (6.01, 2017, 3),
    'AcerAspireVN7571G573Q': (5.18, 2015, 0)
}


def pixels_to_reference(signature: pd.DataFrame, file_params: [str], ref_width: int, ref_height: int) -> pd.DataFrame:
    # Extract relevant data from file name params
    screen_width_px = int(file_params[SCREEN_WIDTH])
    screen_height_px = int(file_params[SCREEN_HEIGHT])

    # Do the normalization of pixel values and speeds using the display data
    signature = signature.apply(lambda x: x * ref_width / screen_width_px if x.name == 'x' else x)
    signature = signature.apply(lambda y: y * ref_height / screen_height_px if y.name == 'y' else y)

    return signature


def pixel_speed_to_reference(signature: pd.DataFrame, file_params: [str], col_names_x_y: [str]) -> pd.DataFrame:
    # Extract relevant data from file name params
    screen_width_dpi = int(file_params[WIDTH_DPI])
    screen_height_dpi = int(file_params[HEIGHT_DPI])

    # Do the normalization of pixel values and speeds using the display data
    signature = signature.apply(lambda vx: (vx * INCH_IN_MM) / screen_width_dpi if vx.name == col_names_x_y[0] else vx)
    signature = signature.apply(lambda vy: (vy * INCH_IN_MM) / screen_height_dpi if vy.name == col_names_x_y[1] else vy)

    return signature


def generate_dataframe(directory: str, callback) -> pd.DataFrame:
    """
    Generates a dataframe from all signatures in the specified folder.
    :param directory: The directory with the signatures to include in the dataframe.
    :param callback: A function that takes a signature as dataframe and the file parameters as
    arguments and returns it as a dataframe with one row with all extracted features.
    :return: Returns a dataframe with one row per signature.
    """

    # Get all signature files in all subdirectories
    signature_files = list()
    for (dirpath, dirnames, filenames) in os.walk(directory, topdown=True):
        signature_files += [os.path.join(dirpath.replace(directory, ""), file)
                            for file in filenames if file.startswith('sig') and file.endswith('.csv')]

    df = None
    included_names = []
    for file in signature_files:

        file_path_params = file.split('/')
        name = file_path_params[0].replace("-", "").casefold()

        session = -1
        if file_path_params[1].startswith('f'):
            session = int(file_path_params[1][1:]) + 2
        else:
            session = int(file_path_params[1])

        file_params = file_path_params[2].split('-')

        writer_id = -1
        if GENERATE_NEW_ID:
            # Find if there is a signature by this person already
            for count, item in enumerate(included_names):
                if item == name:
                    writer_id = count

            if writer_id < 0:
                # If it wasn't in the list, add and use new id
                writer_id = len(included_names)
                included_names.append(name)
        else:
            writer_id = int(name)

        # Load the file
        signature = pd.read_csv(directory + file)

        # Perform feature extraction and transform to 1 row
        df_row = callback(signature, file_params)

        # Add device model indicator
        device_model = file_params[DEVICE_MODEL]
        df_row['devmod'] = [device_model]

        df_row['pos'] = [file_params[RECORDING_POSITION]]
        df_row['sex'] = [file_params[SEX]]
        df_row['age'] = [int(file_params[AGE])]
        df_row['hand'] = [file_params[WRITING_HAND].replace(".csv", "")]
        df_row['ts'] = [file_params[TIMESTAMP]]
        df_row['scrw'] = [int(file_params[SCREEN_WIDTH])]
        df_row['scrh'] = [int(file_params[SCREEN_HEIGHT])]
        df_row['denw'] = [int(file_params[WIDTH_DPI])]
        df_row['denh'] = [int(file_params[HEIGHT_DPI])]
        df_row['country'] = [file_params[COUNTRY]]
        df_row['lang'] = [file_params[LANGUANGE]]
        df_row['dsize'] = [DEVICE_IDENTIFIERS.get(file_params[DEVICE_MODEL])[0]]
        df_row['phrel'] = [DEVICE_IDENTIFIERS.get(file_params[DEVICE_MODEL])[1]]
        df_row['tsQ'] = [DEVICE_IDENTIFIERS.get(file_params[DEVICE_MODEL])[2]]

        df_row['session'] = [session]
        df_row['pID'] = [int(name)]

        # Add genuine indicator
        genuine = 1 if file_params[AUTHENTICITY] == "true" else 0
        df_row['genuine'] = [genuine]

        # Add class
        df_row['label'] = [writer_id]

        # Append to resulting dataframe
        if df is None:
            df = df_row
        else:
            df = pd.concat([df, df_row], ignore_index=True)

    return df
