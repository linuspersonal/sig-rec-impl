# Signature recognition

This repository contains the signature processing toolchain and verification algorithm, all code that was used to generate the results described in the thesis, as well as many supporting scripts and tools to efficiently work with the signature files output by our recording tools.

The code is organized in individual python scripts for individual tasks. The names are quite descriptive, and the code is commented where not self-explanatory to allow for easier use.

