import pandas as pd
import matplotlib.pyplot as plt
import math

# Is the file from the study dataset (with gravity) or the test dataset (without)
# Can be removed once only the study dataset is used
NEW_FILE = True

FILE_DIR_TOUCHPAD = "data/Signatures-Touchpad/"
FILE_DIR_MOBILE = "data/Signatures-Mobile/"
FILE_DIR_MOBILE_LARGE = "data/Signatures-Mobile-New/"
FILE_DIR_MOBILE_OTHER = "data/Signatures-Mobile-Non-S8/"
FILE_DIR_STUDY = "data/study-data-mobile/carla-schnelle/1/"

FILE_DIR = FILE_DIR_STUDY
FILE_NAME = "sig0-Carla-Schnelle-1587996967629-true-1280-720-310-310-SMA320FL-sitting-female-22-Germany -German -right.csv"
# x,y,vX,vY,strokenumber,touchsize,gyroX,gyroY,gyroZ,magX,magY,magZ,accX,accY,accZ,timestamp -> mobile
# signature = pd.read_csv(FILE_DIR + FILE_NAME)
signature = pd.read_csv("data/forgeries-touchpad/armin-hartmann/2/sig0-Armin-Hartmann-1596007215052-true-1236-898-296-292-AcerAspireVN7571G573Q-sitting-male-64-Austria-German-right.csv")

print(signature.info())
print(signature.describe())

# Signature would be mirrored because y axis starts at the top left corner for screen coords, but bottom left here
signature.plot(x='x', y='y', kind='line')
#plt.axis([0, 2175, 0, 1080])
plt.gca().set_aspect('equal')
plt.gca().invert_yaxis()
plt.show()

# The x axis over time
signature.plot(x='timestamp', y='x', kind='line')
plt.show()

# The y axis over time - again, this would be mirrored because of different coordinate systems
signature.plot(x='timestamp', y='y', kind='line')
plt.gca().invert_yaxis()
plt.show()

# Distribution of x coordinates
ax = signature['x'].plot.hist(bins=20)
ax.set_xlabel("X coordinates")
plt.show()

# Distribution of y coordinates
ax = signature['y'].plot.hist(bins=20)
ax.set_xlabel("Y coordinates")
plt.show()


def pythagoras(x, y):
    return math.sqrt(math.pow(x, 2) + math.pow(y, 2))


signature['vTot'] = signature.apply(lambda x: pythagoras(x.vX, x.vY), axis=1)

# Speed
fig, axes = plt.subplots(nrows=2, ncols=2)
signature.plot(ax=axes[0, 0], x='timestamp', y='vX', kind='line')
signature.plot(ax=axes[0, 1], x='timestamp', y='vY', kind='line')
signature.plot(ax=axes[1, 0], x='timestamp', y='vTot', kind='line')
signature.plot(ax=axes[1, 1], x='vX', y='vY', kind='scatter')
plt.show()

# The touchsize over time
signature.plot(x='timestamp', y='touchsize', kind='line')
plt.show()

# The magnetometer values
fig, axes = plt.subplots(nrows=3, ncols=2)
signature.plot(ax=axes[0, 0], x='timestamp', y='magX', kind='line')
signature.plot(ax=axes[0, 1], x='timestamp', y='magY', kind='line')
signature.plot(ax=axes[1, 0], x='timestamp', y='magZ', kind='line')
signature.plot(ax=axes[1, 1], x='magX', y='magY', kind='scatter')
signature.plot(ax=axes[2, 0], x='magX', y='magZ', kind='scatter')
signature.plot(ax=axes[2, 1], x='magY', y='magZ', kind='scatter')
plt.show()

# The accelerometer values
fig, axes = plt.subplots(nrows=3, ncols=2)
signature.plot(ax=axes[0, 0], x='timestamp', y='accX', kind='line')
signature.plot(ax=axes[0, 1], x='timestamp', y='accY', kind='line')
signature.plot(ax=axes[1, 0], x='timestamp', y='accZ', kind='line')
signature.plot(ax=axes[1, 1], x='accX', y='accY', kind='scatter')
signature.plot(ax=axes[2, 0], x='accX', y='accZ', kind='scatter')
signature.plot(ax=axes[2, 1], x='accY', y='accZ', kind='scatter')
plt.show()

# The gyroscope values
fig, axes = plt.subplots(nrows=3, ncols=2)
signature.plot(ax=axes[0, 0], x='timestamp', y='gyroX', kind='line')
signature.plot(ax=axes[0, 1], x='timestamp', y='gyroY', kind='line')
signature.plot(ax=axes[1, 0], x='timestamp', y='gyroZ', kind='line')
signature.plot(ax=axes[1, 1], x='gyroX', y='gyroY', kind='scatter')
signature.plot(ax=axes[2, 0], x='gyroX', y='gyroZ', kind='scatter')
signature.plot(ax=axes[2, 1], x='gyroY', y='gyroZ', kind='scatter')
plt.show()

if NEW_FILE:
    fig, axes = plt.subplots(nrows=3, ncols=2)
    signature.plot(ax=axes[0, 0], x='timestamp', y='gravX', kind='line')
    signature.plot(ax=axes[0, 1], x='timestamp', y='gravY', kind='line')
    signature.plot(ax=axes[1, 0], x='timestamp', y='gravZ', kind='line')
    signature.plot(ax=axes[1, 1], x='gravX', y='gravY', kind='scatter')
    signature.plot(ax=axes[2, 0], x='gravX', y='gravZ', kind='scatter')
    signature.plot(ax=axes[2, 1], x='gravY', y='gravZ', kind='scatter')
    plt.show()