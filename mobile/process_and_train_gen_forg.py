from statistics import mean

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import balanced_accuracy_score, make_scorer, confusion_matrix, plot_confusion_matrix
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from util.process_and_train import combine_label_and_genuine, print_grid_search_results

# Constants

STATISTICAL_INFO_WITHOUT_GENUINE = [
    'devmod', 'pos', 'sex', 'age', 'hand', 'session',
    'scrw', 'scrh', 'denw', 'denh', 'country', 'lang', 'dsize', 'phrel', 'tsQ', 'pID', 'ts'
]
GENUINE = ['genuine']
REDUNDANT_INFO = ['vX_', 'vY_']

GLOBAL_INFO = ['duration', 'nrStrokes']

LOCAL_GEOM = [
    'x_', 'y_', 'fc_', 'dX', 'dY',
    'pta', 'dpta', 'pvm', 'dpvm', 'pvmr5',
    'lcr', 'dlcr', 'tam', 'dtam',
    'ddX', 'ddY', 'acs', 'dacs', 'sinacs', 'cosacs',
    'ltwr5', 'ltwr7',]
GLOBAL_GEOM = [
    'startX', 'startY', 'endX', 'endY',
    'minX', 'minY', 'maxX', 'maxY',
    'widthX', 'heightY', 'size', 'maxdX', 'maxdY',
    'mx', 'my', 'mdX', 'mdY',
    'sdx', 'sdy', 'sddX', 'sddY',
    'skx', 'sky', 'skdX', 'skdY',
    'ktx', 'kty', 'ktdX', 'ktdY'
]

LOCAL_TOUCHSIZE = ['touchsize']
GLOBAL_TOUCHSIZE = ['mtouchsize', 'sdtouchsize', 'sktouchsize', 'kttouchsize']

LOCAL_MAG = ['mag']
GLOBAL_MAG = ['mmag', 'sdmag', 'skmag', 'ktmag']

LOCAL_ACC = ['acc']
GLOBAL_ACC = ['macc', 'sdacc', 'skacc', 'ktacc']

LOCAL_GYRO = ['gyro']
GLOBAL_GYRO = ['mgyro', 'sdgyro', 'skgyro', 'ktgyro']

LOCAL_GRAV = ['grav']
GLOBAL_GRAV = ['mgrav', 'sdgrav', 'skgrav', 'ktgrav']

LABEL = ['label']

# Feature shorthands
ALWAYS_REMOVE = STATISTICAL_INFO_WITHOUT_GENUINE + REDUNDANT_INFO

GLOBAL_COMBINED = GLOBAL_INFO + GLOBAL_GEOM + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_MOVEMENT = GLOBAL_INFO + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_XY = GLOBAL_INFO + GLOBAL_GEOM

LOCAL_COMBINED = LOCAL_GEOM + LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_MOVEMENT = LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_XY = LOCAL_GEOM

GL_COMBINED = GLOBAL_COMBINED + LOCAL_COMBINED
GL_MOVEMENT = GLOBAL_MOVEMENT + LOCAL_MOVEMENT
GL_XY = GLOBAL_XY + LOCAL_XY

# ----- Settings -----

FILE_DIR = "data/dataset-mobile/"
FILE_NAME = "dataset.csv"

RUN_GRIDSEARCH = False
RUN_LEAVE_SUBJ_OUT_CV = True

FEATURES = GLOBAL_MOVEMENT

# xy, movement

PLOT_OUTPUT_DIR = "plots/forgery-detector/mobile/"

# ------ Start of script -----

dataFrame = pd.read_csv(FILE_DIR + FILE_NAME)

print(dataFrame.info())
print(dataFrame.describe())
class_count = dataFrame['label'].max() + 1

# Combine label and genuine columns
dataFrame = combine_label_and_genuine(dataFrame, 'label', 'genuine')

# dataFrame = dataFrame.loc[(dataFrame['pos'].str.match('sitting'))]

# Remove
cols = [c for c in dataFrame.columns if not c.startswith(tuple(ALWAYS_REMOVE))]
dataFrame = dataFrame[cols]

# Select features to use
FEATURES = FEATURES + LABEL + GENUINE
cols = [c for c in dataFrame.columns if c.startswith(tuple(FEATURES))]
dataFrame = dataFrame[cols]

# ------ Start of ML ---------

if RUN_LEAVE_SUBJ_OUT_CV:
    # Leave subject out
    clf = RandomForestClassifier(max_depth=30, max_features='sqrt', n_estimators=200, n_jobs=-1) # Combined
    # clf = RandomForestClassifier(max_depth=10, max_features='log2', n_estimators=50, n_jobs=-1) # XY
    # clf = GaussianNB(var_smoothing=1e-10)
    # clf = DecisionTreeClassifier(max_depth=5, max_features=None)
    # clf = SVC(kernel='rbf', gamma=0.0005)
    # clf = SVC(kernel='linear', C=5)
    # clf = KNeighborsClassifier(n_neighbors=10)
    # clf = AdaBoostClassifier(learning_rate=0.5, n_estimators=100) # XY, Combined
    # clf = MLPClassifier(activation='logistic', alpha=0.01, max_iter=2000, solver='adam')

    conf_matrix_all = np.array([[0, 0], [0, 0]])
    results = []
    iteration_count = 0
    for i in range(0, class_count):
        print("Iteration", iteration_count)
        iteration_count += 1
        # Only include one class and its forgeries
        test_leave_out = dataFrame.query('label == {0} | label == {1}'.format(i, i+class_count))
        test_leave_out = test_leave_out.reset_index(drop=True)
        test_leave_out = test_leave_out.drop(['label'], axis=1)
        test_leave_out = test_leave_out.rename(columns={"genuine": "label"})

        train_leave_out = dataFrame.query('label != {0} & label != {1}'.format(i, i+class_count))
        train_leave_out = train_leave_out.reset_index(drop=True)
        train_leave_out = train_leave_out.drop(['label'], axis=1)  # Not needed for now
        train_leave_out = train_leave_out.rename(columns={"genuine": "label"})

        X_test = test_leave_out.drop('label', axis=1)
        y_test = test_leave_out['label']
        X_train = train_leave_out.drop('label', axis=1)
        y_train = train_leave_out['label']

        clf.fit(X_train, y_train)

        preds = clf.predict(X_test)
        disp = plot_confusion_matrix(clf, X_test, y_test, display_labels=["forgery", "genuine"])
        disp.ax_.set_title("Genuine/Forgery confusion matrix")
        plt.savefig(PLOT_OUTPUT_DIR + "conf-matrix-label-" + str(i) + ".pdf")

        cur_conf_matrix = confusion_matrix(y_test, preds)
        print("Current conf matrix:")
        print(cur_conf_matrix)
        conf_matrix_all = np.add(conf_matrix_all, cur_conf_matrix)
        print("Summed up conf matrix so far:")
        print(conf_matrix_all)

        score = balanced_accuracy_score(y_test, preds, adjusted=False)
        results.append(score)

    print("Total iterations: ", iteration_count)

    for result in results:
        print("Score: ", result)

    overall = mean(results)
    print("Overall Score: ", overall)

    print(conf_matrix_all)
    avg_matrix = np.divide(conf_matrix_all, iteration_count)
    print(avg_matrix)
    tn, fp, fn, tp = avg_matrix.ravel()
    fpr = fp / (fp + tn)
    fnr = fn / (fn + tp)
    print("FPR:", fpr)  # False Acceptance Rate
    print("FNR:", fnr)  # False Rejection Rate
    print()

if RUN_GRIDSEARCH:
    dataFrame = dataFrame.drop(['label'], axis=1)  # Not needed for now
    dataFrame = dataFrame.rename(columns={"genuine": "label"})

    X = dataFrame.drop('label', axis=1)
    y = dataFrame['label']

    names = ["Nearest Neighbors",
             "Linear SVM",
             "RBF SVM",
             "Decision Tree",
             "Random Forest",
             "Neural Net",
             "AdaBoost",
             "Naive Bayes"]

    parameters = [{'n_neighbors': [1, 2, 3, 4, 5, 7, 10]},
                  {'kernel': ['linear'], 'C': [0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50, 100]},
                  {'kernel': ['rbf'], 'gamma': [0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 2, 5]},
                  {'max_depth': [2, 3, 5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None]},
                  {'max_depth': [2, 3, 5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None], 'n_estimators': [20, 50, 100, 200]},
                  {'activation': ['logistic', 'tanh', 'relu'], 'solver': ['lbfgs', 'adam'], 'alpha': [0.0001, 0.001, 0.01, 0.1, 1, 10], 'max_iter': [2000]},
                  {'n_estimators': [10, 20, 50, 100], 'learning_rate': [0.1, 0.5, 1.0, 5]},
                  {'var_smoothing': [1e-10, 1e-9, 1e-8]}]

    classifiers = [
        KNeighborsClassifier(),
        SVC(),
        SVC(),
        DecisionTreeClassifier(),
        RandomForestClassifier(),
        MLPClassifier(),
        AdaBoostClassifier(),
        GaussianNB()]

    scorer = make_scorer(balanced_accuracy_score, adjusted=False)

    for name, params, clf in zip(names, parameters, classifiers):
        gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5, n_jobs=-1)
        gs_model.fit(X, y)

        print_grid_search_results(gs_model, name)
