import pandas as pd
import numpy as np
import statistics
from sklearn.metrics import make_scorer
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.inspection import permutation_importance
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from util.process_and_train import print_grid_search_results, combine_label_and_genuine, normalize_colwise

# Constants

STATISTICAL_INFO = [
    'devmod', 'genuine', 'pos', 'sex', 'age', 'hand', 'session',
    'scrw', 'scrh', 'denw', 'denh', 'country', 'lang', 'dsize', 'phrel', 'tsQ', 'pID', 'ts'
]
REDUNDANT_INFO = ['vX_', 'vY_']

GLOBAL_INFO = ['duration', 'nrStrokes']

LOCAL_GEOM = [
    'x_', 'y_', 'fc_', 'dX', 'dY',
    'pta', 'dpta', 'pvm', 'dpvm', 'pvmr5',
    'lcr', 'dlcr', 'tam', 'dtam',
    'ddX', 'ddY', 'acs', 'dacs', 'sinacs', 'cosacs',
    'ltwr5', 'ltwr7',]
GLOBAL_GEOM = [
    'startX', 'startY', 'endX', 'endY',
    'minX', 'minY', 'maxX', 'maxY',
    'widthX', 'heightY', 'size', 'maxdX', 'maxdY',
    'mx', 'my', 'mdX', 'mdY',
    'sdx', 'sdy', 'sddX', 'sddY',
    'skx', 'sky', 'skdX', 'skdY',
    'ktx', 'kty', 'ktdX', 'ktdY'
]

LOCAL_TOUCHSIZE = ['touchsize']
GLOBAL_TOUCHSIZE = ['mtouchsize', 'sdtouchsize', 'sktouchsize', 'kttouchsize']

LOCAL_MAG = ['mag']
GLOBAL_MAG = ['mmag', 'sdmag', 'skmag', 'ktmag']

LOCAL_ACC = ['acc']
GLOBAL_ACC = ['macc', 'sdacc', 'skacc', 'ktacc']

LOCAL_GYRO = ['gyro']
GLOBAL_GYRO = ['mgyro', 'sdgyro', 'skgyro', 'ktgyro']

LOCAL_GRAV = ['grav']
GLOBAL_GRAV = ['mgrav', 'sdgrav', 'skgrav', 'ktgrav']

LABEL = ['label']

# Feature shorthands
ALWAYS_REMOVE = STATISTICAL_INFO + REDUNDANT_INFO

GLOBAL_COMBINED = GLOBAL_INFO + GLOBAL_GEOM + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_MOVEMENT = GLOBAL_INFO + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_XY = GLOBAL_INFO + GLOBAL_GEOM

LOCAL_COMBINED = LOCAL_GEOM + LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_MOVEMENT = LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_XY = LOCAL_GEOM

GL_COMBINED = GLOBAL_COMBINED + LOCAL_COMBINED
GL_MOVEMENT = GLOBAL_MOVEMENT + LOCAL_MOVEMENT
GL_XY = GLOBAL_XY + LOCAL_XY

# ----------- Settings ---------------

FILE_DIR = "data/dataset-mobile/"
FILE_NAME = "dataset.csv"

DIFFERENT_LABELS_FOR_FORGERIES = True

USE_RANDOM_FORGERIES = True        # If false, use skilled
PERMUTATION_IMPORTANCE = True

FEATURES = GLOBAL_COMBINED

# ----------- Start of script ----------

dataFrame = pd.read_csv(FILE_DIR + FILE_NAME)

print(dataFrame.info())
print(dataFrame.describe())

number_of_genuine_classes = dataFrame['label'].max() + 1

if DIFFERENT_LABELS_FOR_FORGERIES:
    dataFrame = combine_label_and_genuine(dataFrame, 'label', 'genuine')

# Remove
cols = [c for c in dataFrame.columns if not c.startswith(tuple(ALWAYS_REMOVE))]
dataFrame = dataFrame[cols]

# Select features to use
FEATURES = FEATURES + LABEL
cols = [c for c in dataFrame.columns if c.startswith(tuple(FEATURES))]
dataFrame = dataFrame[cols]

# Test different classifiers
names = [
         "Random Forest",
         ]

parameters = [
              {'max_depth': [2, 3, 5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None],
               'n_estimators': [20, 50, 100, 200]},
              ]

classifiers = [
    RandomForestClassifier(),
]

scorer = make_scorer(balanced_accuracy_score, adjusted=False)

# Start ML

score_list = []

feature_importance = [0] * (dataFrame.shape[1]-1)
permutation_importance_means = np.zeros(dataFrame.shape[1]-1)
permutation_importance_stds = np.zeros(dataFrame.shape[1]-1)
count = 0

for cur_label in range(0, number_of_genuine_classes):
    print("Cur label: " + str(cur_label) + ", Iteration: " + str(count))
    count += 1

    # Dateset without the forgeries for the class under test
    dfNoForgeries = dataFrame.query('label != {0}'.format(cur_label + number_of_genuine_classes))
    dfNoForgeries = dfNoForgeries.reset_index(drop=True)

    # Everything that is not the current label is 0
    dfNoForgeries['label'] = dfNoForgeries['label'].apply(lambda label: 1 if label == cur_label else 0)

    # Split in only current positive and random forgeries
    dfGenuineOnly = dfNoForgeries.query('label == 1')
    dfGenuineOnly = dfGenuineOnly.reset_index(drop=True)
    dfRandomForgOnly = dfNoForgeries.query('label == 0')
    dfRandomForgOnly = dfRandomForgOnly.reset_index(drop=True)

    # Dataset with only the forgeries of the class under test
    dfForgeriesOnly = dataFrame.query('label == {0}'.format(cur_label + number_of_genuine_classes))
    dfForgeriesOnly = dfForgeriesOnly.reset_index(drop=True)

    # Make it negative class
    dfForgeriesOnly['label'] = 0

    hold_out_positive = dfGenuineOnly.sample(20)
    if PERMUTATION_IMPORTANCE:
        dfGenuineOnly = dfGenuineOnly.drop(hold_out_positive.index)

    if USE_RANDOM_FORGERIES:
        train = dfGenuineOnly.append(dfRandomForgOnly, ignore_index=True)
    else:
        train = dfGenuineOnly.append(dfForgeriesOnly, ignore_index=True)

    X_train = train.drop('label', axis=1)
    Y_train = train['label']

    cur_scores = []
    for name, params, clf in zip(names, parameters, classifiers):
        gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5, n_jobs=-1)
        gs_model.fit(X_train, Y_train)

        print_grid_search_results(gs_model, name)

        if PERMUTATION_IMPORTANCE:
            permutation_test = hold_out_positive.append(dfForgeriesOnly, ignore_index=True)
            permutation_test_X = permutation_test.drop('label', axis=1)
            permutation_test_Y = permutation_test['label']

            permutation_result = permutation_importance(gs_model.best_estimator_, permutation_test_X, permutation_test_Y, n_repeats=5, random_state=42, n_jobs=-1)

            permutation_importance_means = permutation_importance_means + permutation_result.importances_mean
            permutation_importance_stds = permutation_importance_stds + permutation_result.importances_std
            col_names = permutation_test_X.columns.tolist()
            print("permutation feature importance (latest average)")

            indices = permutation_importance_means.argsort()[::-1]
            for f in range(permutation_test_X.shape[1]):
                print("%d. %s [%s] %f (%f)" % (
                f + 1, col_names[indices[f]], indices[f], permutation_importance_means[indices[f]] / count, permutation_importance_stds[indices[f]] / count))

            print('test')

        else:
            feature_importance = [(x + y) for x, y in zip(feature_importance, gs_model.best_estimator_.feature_importances_)]
            col_names = X_train.columns.tolist()
            print("grid search feature importance (latest average)")

            indices = np.argsort(feature_importance)[::-1]
            for f in range(X_train.shape[1]):
                print("%d. %s [%s] (%f)" % (f + 1, col_names[indices[f]], indices[f], feature_importance[indices[f]]/count))

        cur_scores.append(gs_model.best_score_)

    score_list.append(cur_scores)

score_list = np.array(score_list).T.tolist()
for i in range(0, len(names)):
    print("Best scores of " + names[i] + " per subject")
    print("Individual: " + str(score_list[i]))
    print("Mean: " + str(statistics.mean(score_list[i])))
    print("StDev: " + str(statistics.stdev(score_list[i])))
    print("")
