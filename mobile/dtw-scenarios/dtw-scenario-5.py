from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output
from util.dynamic_time_warping import OUTPUT_DIR_MOBILE, FILE_DIR_MOBILE, FILE_NAME_MOBILE, \
    FEATURES_XY, FEATURES_MOVEMENT, FEATURES_COMBINED

import pandas as pd

SKILLED_FORGERIES = False

df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

# ----------------- Scenario 5 -------------------
dir_name = OUTPUT_DIR_MOBILE + "dtw-scenario-5/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"

file_name = "dtw-5-sitting-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-sitting-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-sitting-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-standing-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-standing-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-standing-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-walking-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-walking-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-5-walking-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting', 'standing', 'walking'],
                           samples_train_first=[2, 2, 2],
                           samples_train_second=[0, 0, 0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)