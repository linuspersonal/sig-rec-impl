from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output, FEATURES_ACC, \
    FEATURES_GRAV, FEATURES_GYRO, FEATURE_TOUCHSIZE
from util.dynamic_time_warping import OUTPUT_DIR_MOBILE, FILE_DIR_MOBILE, FILE_NAME_MOBILE

import pandas as pd

SKILLED_FORGERIES = True

SUBSET = 'all' # 'all', 'tsq3', 'smg950f'

# Start script

df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

# Select only skilled forgeries with tsQ == 3; and only those for subjects in the genuine tsQ == 3 set
df_mobile_tsq3_gen = df_mobile.loc[(df_mobile['tsQ'] == 3) & (df_mobile['genuine'] == 1)]
df_mobile_tsq3_forg = df_mobile.loc[(df_mobile['tsQ'] == 3) & (df_mobile['genuine'] == 0)]
df_mobile_tsq3_forg = df_mobile_tsq3_forg[df_mobile_tsq3_forg['label'].isin(df_mobile_tsq3_gen['label'])]
df_mobile_tsq3 = df_mobile_tsq3_gen.append(df_mobile_tsq3_forg)
tsq3_counts = df_mobile_tsq3.groupby('label')['label'].count()      # Verify number of signatures per subject

# Select only data from SMG950F device
df_mobile_smg950f_gen = df_mobile.loc[(df_mobile['devmod'] == 'SMG950F') & (df_mobile['genuine'] == 1)]
df_mobile_smg950f_forg = df_mobile.loc[(df_mobile['devmod'] == 'SMG950F') & (df_mobile['genuine'] == 0)]
df_mobile_smg950f_forg = df_mobile_smg950f_forg[df_mobile_smg950f_forg['label'].isin(df_mobile_smg950f_gen['label'])]
df_mobile_smg950f = df_mobile_smg950f_gen.append(df_mobile_smg950f_forg)
smg950f_counts = df_mobile_smg950f.groupby('label')['label'].count()    # Verify number of signatures per subject

appendix = ""
samples_test = 10

if SUBSET == 'tsq3':
    df_mobile = df_mobile_tsq3
    appendix = "-" + SUBSET
    samples_test = 5
elif SUBSET == 'smg950f':
    df_mobile = df_mobile_smg950f
    appendix = "-" + SUBSET
    samples_test = 5

# ----------------- Scenario 3 -------------------
dir_name = OUTPUT_DIR_MOBILE + "dtw-scenario-3-sf" + appendix + "/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"


if SUBSET != 'all':
    file_name = "dtw-3-sitting-touchsize" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURE_TOUCHSIZE,
                               stance_train=['sitting'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['sitting'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=True,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)


    file_name = "dtw-3-standing-touchsize" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURE_TOUCHSIZE,
                               stance_train=['standing'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['standing'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=True,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)


    file_name = "dtw-3-walking-touchsize" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURE_TOUCHSIZE,
                               stance_train=['walking'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['walking'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=True,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-sitting-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_ACC,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-sitting-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GRAV,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-sitting-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GYRO,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-standing-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_ACC,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-standing-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GRAV,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-standing-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GYRO,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-walking-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_ACC,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-walking-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GRAV,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-3-walking-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_GYRO,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=True,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)