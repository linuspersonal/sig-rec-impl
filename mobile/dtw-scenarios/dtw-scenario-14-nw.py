from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output
from util.dynamic_time_warping import OUTPUT_DIR_MOBILE, FILE_DIR_MOBILE, FILE_NAME_MOBILE, \
    FEATURES_XY, FEATURES_MOVEMENT, FEATURES_COMBINED

import pandas as pd

SKILLED_FORGERIES = True

df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

# ----------------- Scenario 14 -------------------
dir_name = OUTPUT_DIR_MOBILE + "dtw-scenario-14-no-walking/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"

file_name = "dtw-14-multi-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting', 'standing'],
                           samples_train_first=[1, 2],
                           samples_train_second=[2, 1],
                           stance_test=['sitting', 'standing'],
                           samples_test_first=[5, 5],
                           samples_test_second=[5, 5],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-14-multi-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting', 'standing'],
                           samples_train_first=[1, 2],
                           samples_train_second=[2, 1],
                           stance_test=['sitting', 'standing'],
                           samples_test_first=[5, 5],
                           samples_test_second=[5, 5],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-14-multi-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting', 'standing'],
                           samples_train_first=[1, 2],
                           samples_train_second=[2, 1],
                           stance_test=['sitting', 'standing'],
                           samples_test_first=[5, 5],
                           samples_test_second=[5, 5],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)
