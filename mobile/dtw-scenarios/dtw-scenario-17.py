from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output
from util.dynamic_time_warping import OUTPUT_DIR_MOBILE, FILE_DIR_MOBILE, FILE_NAME_MOBILE, \
    FEATURES_XY, FEATURES_MOVEMENT, FEATURES_COMBINED

import pandas as pd

SKILLED_FORGERIES = False

df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

# ----------------- Scenario 17 -------------------
dir_name = OUTPUT_DIR_MOBILE + "dtw-scenario-17/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"

file_name = "dtw-17-sitting-standing-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-sitting-standing-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-sitting-standing-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-sitting-walking-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-sitting-walking-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-sitting-walking-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)




file_name = "dtw-17-standing-sitting-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-standing-sitting-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-standing-sitting-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-standing-walking-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-standing-walking-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-standing-walking-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)




file_name = "dtw-17-walking-sitting-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-walking-sitting-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-walking-sitting-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)

file_name = "dtw-17-walking-standing-xy.txt"
result = classify_with_dtw(df_mobile, FEATURES_XY,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-walking-standing-movement.txt"
result = classify_with_dtw(df_mobile, FEATURES_MOVEMENT,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-17-walking-standing-combined.txt"
result = classify_with_dtw(df_mobile, FEATURES_COMBINED,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[10],
                           samples_test_second=[10],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)