from util.dynamic_time_warping import classify_with_dtw, dtw_results_to_string, write_dtw_output, FEATURES_XY_TS, \
    FEATURES_XY_ACC, FEATURES_XY_GRAV, FEATURES_XY_GYRO
from util.dynamic_time_warping import OUTPUT_DIR_MOBILE, FILE_DIR_MOBILE, FILE_NAME_MOBILE, \
    FEATURES_XY, FEATURES_MOVEMENT, FEATURES_COMBINED

import pandas as pd

SKILLED_FORGERIES = False

SUBSET = 'all' # 'all', 'tsq3', 'smg950f'

# Start script

df_mobile = pd.read_csv(FILE_DIR_MOBILE + FILE_NAME_MOBILE)

# Select only skilled forgeries with tsQ == 3; and only those for subjects in the genuine tsQ == 3 set
df_mobile_tsq3_gen = df_mobile.loc[(df_mobile['tsQ'] == 3) & (df_mobile['genuine'] == 1)]
df_mobile_tsq3_forg = df_mobile.loc[(df_mobile['tsQ'] == 3) & (df_mobile['genuine'] == 0)]
df_mobile_tsq3_forg = df_mobile_tsq3_forg[df_mobile_tsq3_forg['label'].isin(df_mobile_tsq3_gen['label'])]
df_mobile_tsq3 = df_mobile_tsq3_gen.append(df_mobile_tsq3_forg)
tsq3_counts = df_mobile_tsq3.groupby('label')['label'].count()      # Verify number of signatures per subject

# Select only data from SMG950F device
df_mobile_smg950f_gen = df_mobile.loc[(df_mobile['devmod'] == 'SMG950F') & (df_mobile['genuine'] == 1)]
df_mobile_smg950f_forg = df_mobile.loc[(df_mobile['devmod'] == 'SMG950F') & (df_mobile['genuine'] == 0)]
df_mobile_smg950f_forg = df_mobile_smg950f_forg[df_mobile_smg950f_forg['label'].isin(df_mobile_smg950f_gen['label'])]
df_mobile_smg950f = df_mobile_smg950f_gen.append(df_mobile_smg950f_forg)
smg950f_counts = df_mobile_smg950f.groupby('label')['label'].count()    # Verify number of signatures per subject

appendix = ""
samples_test = 10

if SUBSET == 'tsq3':
    df_mobile = df_mobile_tsq3
    appendix = "-" + SUBSET
    samples_test = 5
elif SUBSET == 'smg950f':
    df_mobile = df_mobile_smg950f
    appendix = "-" + SUBSET
    samples_test = 5

# ----------------- Scenario 1 -------------------
dir_name = OUTPUT_DIR_MOBILE + "dtw-scenario-1-xy-ind" + appendix + "/"
dir_name = dir_name + "skilled/" if SKILLED_FORGERIES else dir_name + "random/"

file_name = "dtw-1-sitting-xy-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_ACC,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-sitting-xy-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GRAV,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-sitting-xy-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GYRO,
                           stance_train=['sitting'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['sitting'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-standing-xy-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_ACC,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-standing-xy-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GRAV,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-standing-xy-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GYRO,
                           stance_train=['standing'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['standing'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-walking-xy-acc" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_ACC,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-walking-xy-grav" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GRAV,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)


file_name = "dtw-1-walking-gyro" + appendix + ".txt"
result = classify_with_dtw(df_mobile, FEATURES_XY_GYRO,
                           stance_train=['walking'],
                           samples_train_first=[5],
                           samples_train_second=[0],
                           stance_test=['walking'],
                           samples_test_first=[samples_test],
                           samples_test_second=[samples_test],
                           random_train=False,
                           skilled_forgeries=SKILLED_FORGERIES)

output = dtw_results_to_string(df_mobile, result)
write_dtw_output(dir_name, file_name, output, True)

# ------------- Including Touchsize -------------

if SUBSET == 'tsq3' or SUBSET == 'smg950f':
    file_name = "dtw-1-sitting-xy-ts" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURES_XY_TS,
                               stance_train=['sitting'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['sitting'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=False,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)

    file_name = "dtw-1-standing-xy-ts" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURES_XY_TS,
                               stance_train=['standing'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['standing'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=False,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)

    file_name = "dtw-1-walking-xy-ts" + appendix + ".txt"
    result = classify_with_dtw(df_mobile, FEATURES_XY_TS,
                               stance_train=['walking'],
                               samples_train_first=[5],
                               samples_train_second=[0],
                               stance_test=['walking'],
                               samples_test_first=[samples_test],
                               samples_test_second=[samples_test],
                               random_train=False,
                               skilled_forgeries=SKILLED_FORGERIES)

    output = dtw_results_to_string(df_mobile, result)
    write_dtw_output(dir_name, file_name, output, True)