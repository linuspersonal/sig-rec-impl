# Remove forced sklearn warnings
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import pandas as pd
import numpy as np
import statistics
from sklearn.metrics import make_scorer
from sklearn.metrics import balanced_accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from util.process_and_train import print_grid_search_results, combine_label_and_genuine, normalize_colwise

# Constants

STATISTICAL_INFO = [
    'devmod', 'genuine', 'pos', 'sex', 'age', 'hand', 'session',
    'scrw', 'scrh', 'denw', 'denh', 'country', 'lang', 'dsize', 'phrel', 'tsQ', 'pID', 'ts'
]
REDUNDANT_INFO = ['vX_', 'vY_']

GLOBAL_INFO = ['duration', 'nrStrokes']

LOCAL_GEOM = [
    'x_', 'y_', 'fc_', 'dX', 'dY',
    'pta', 'dpta', 'pvm', 'dpvm', 'pvmr5',
    'lcr', 'dlcr', 'tam', 'dtam',
    'ddX', 'ddY', 'acs', 'dacs', 'sinacs', 'cosacs',
    'ltwr5', 'ltwr7',]
GLOBAL_GEOM = [
    'startX', 'startY', 'endX', 'endY',
    'minX', 'minY', 'maxX', 'maxY',
    'widthX', 'heightY', 'size', 'maxdX', 'maxdY',
    'mx', 'my', 'mdX', 'mdY',
    'sdx', 'sdy', 'sddX', 'sddY',
    'skx', 'sky', 'skdX', 'skdY',
    'ktx', 'kty', 'ktdX', 'ktdY'
]

LOCAL_TOUCHSIZE = ['touchsize']
GLOBAL_TOUCHSIZE = ['mtouchsize', 'sdtouchsize', 'sktouchsize', 'kttouchsize']

LOCAL_MAG = ['mag']
GLOBAL_MAG = ['mmag', 'sdmag', 'skmag', 'ktmag']

LOCAL_ACC = ['acc']
GLOBAL_ACC = ['macc', 'sdacc', 'skacc', 'ktacc']

LOCAL_GYRO = ['gyro']
GLOBAL_GYRO = ['mgyro', 'sdgyro', 'skgyro', 'ktgyro']

LOCAL_GRAV = ['grav']
GLOBAL_GRAV = ['mgrav', 'sdgrav', 'skgrav', 'ktgrav']

LABEL = ['label']

# Feature shorthands
ALWAYS_REMOVE = STATISTICAL_INFO + REDUNDANT_INFO

GLOBAL_COMBINED = GLOBAL_INFO + GLOBAL_GEOM + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_MOVEMENT = GLOBAL_INFO + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_XY = GLOBAL_INFO + GLOBAL_GEOM

LOCAL_COMBINED = LOCAL_GEOM + LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_MOVEMENT = LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_XY = LOCAL_GEOM

GL_COMBINED = GLOBAL_COMBINED + LOCAL_COMBINED
GL_MOVEMENT = GLOBAL_MOVEMENT + LOCAL_MOVEMENT
GL_XY = GLOBAL_XY + LOCAL_XY

# ----------- Settings ---------------

FILE_DIR = "data/dataset-touchpad/"
FILE_NAME = "dataset.csv"

DIFFERENT_LABELS_FOR_FORGERIES = True

NR_RF_TRAIN = 1500
NR_GENUINE_TRAIN = [10, 20]

NR_RF_TEST = 20
NR_SK_TEST = 20
NR_GEN_TEST = 20

FEATURES = GLOBAL_XY

# ----------- Start of script ----------

dataFrame = pd.read_csv(FILE_DIR + FILE_NAME)

print(dataFrame.info())
print(dataFrame.describe())

number_of_genuine_classes = dataFrame['label'].max() + 1

if DIFFERENT_LABELS_FOR_FORGERIES:
    dataFrame = combine_label_and_genuine(dataFrame, 'label', 'genuine')

# Remove
cols = [c for c in dataFrame.columns if not c.startswith(tuple(ALWAYS_REMOVE))]
dataFrame = dataFrame[cols]

# Select features to use
FEATURES = FEATURES + LABEL
cols = [c for c in dataFrame.columns if c.startswith(tuple(FEATURES))]
dataFrame = dataFrame[cols]

# Test different classifiers
names = ["Nearest Neighbors",
         "Linear SVM",
         "RBF SVM",
         "Decision Tree",
         "Random Forest",
         "Neural Net",
         "AdaBoost",
         "Naive Bayes"
    ]

parameters = [{'n_neighbors': [1, 2, 3, 4, 5, 7, 10]},
              {'kernel': ['linear'], 'C': [0.01, 0.1, 1, 10, 100]},
              {'kernel': ['rbf'], 'gamma': [0.00001, 0.00005, 0.0001, 0.0005]},
              {'max_depth': [5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None]},
              {'max_depth': [5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None],
               'n_estimators': [20, 50, 100, 200]},
              [{'activation': ['logistic', 'tanh'], 'solver': ['lbfgs'], 'alpha': [0.0001, 0.001, 0.01, 0.1, 1, 10], 'max_iter': [2000]},
               {'activation': ['relu'], 'solver': ['adam'], 'alpha': [0.0001, 0.001, 0.01, 0.1, 1, 10], 'max_iter': [2000]}],
              {'n_estimators': [50, 100], 'learning_rate': [0.5, 1.0]},
              {'var_smoothing': [1e-10, 1e-9, 1e-8]}
    ]

classifiers = [
    KNeighborsClassifier(),
    SVC(),
    SVC(),
    DecisionTreeClassifier(),
    RandomForestClassifier(),
    MLPClassifier(),
    AdaBoostClassifier(),
    GaussianNB()
    ]

scorer = make_scorer(balanced_accuracy_score, adjusted=False)

# Start classification

score_list = []
rf_matrices = []
sk_matrices = []
best_params = []

for cur_label in range(0, number_of_genuine_classes):

    print("Label:", cur_label)

    # Only genuine signatures
    dfNoForgeries = dataFrame.query('label < {0}'.format(number_of_genuine_classes))
    dfNoForgeries = dfNoForgeries.reset_index(drop=True)

    # Everything that is not the current label is 0
    dfNoForgeries['label'] = dfNoForgeries['label'].apply(lambda label: 1 if label == cur_label else 0)

    # Split in only current positive and random forgeries
    dfGenuineOnly = dfNoForgeries.query('label == 1')
    dfGenuineOnly = dfGenuineOnly.reset_index(drop=True)
    dfRandomForgOnly = dfNoForgeries.query('label == 0')
    dfRandomForgOnly = dfRandomForgOnly.reset_index(drop=True)

    X_no_forg = dfNoForgeries.drop('label', axis=1)
    y_no_forg = dfNoForgeries['label']

    # Dataset with only the forgeries of the class under test
    dfForgeriesOnly = dataFrame.query('label == {0}'.format(cur_label + number_of_genuine_classes))
    dfForgeriesOnly = dfForgeriesOnly.reset_index(drop=True)

    # Make it negative class
    dfForgeriesOnly['label'] = 0

    X_forg_only = dfForgeriesOnly.drop('label', axis=1)
    y_forg_only = dfForgeriesOnly['label']

    cur_scores = []
    subj_rf_matrices = []
    subj_sk_matrices = []
    subj_best_params = []
    for name, params, clf in zip(names, parameters, classifiers):
        gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5, n_jobs=-1)
        gs_model.fit(X_no_forg, y_no_forg)

        print_grid_search_results(gs_model, name)

        cur_scores.append(gs_model.best_score_)
        subj_best_params.append(gs_model.best_params_)

        cur_rf_matrices = []
        cur_sk_matrices = []

        for i in range(0, len(NR_GENUINE_TRAIN)):

            nr_genuine = NR_GENUINE_TRAIN[i]

            positive_train = dfGenuineOnly.sample(nr_genuine)
            positive_others = dfGenuineOnly.drop(positive_train.index)
            positive_test = positive_others.sample(NR_GEN_TEST)

            random_forg_train = dfRandomForgOnly.sample(NR_RF_TRAIN)
            random_forg_others = dfRandomForgOnly.drop(random_forg_train.index)
            random_forg_test = random_forg_others.sample(NR_RF_TEST)

            train = positive_train.append(random_forg_train, ignore_index=True)
            test_rf = positive_test.append(random_forg_test, ignore_index=True)
            test_sk = positive_test.append(dfForgeriesOnly, ignore_index=True)

            train_X = train.drop('label', axis=1)
            train_Y = train['label']

            test_rf_X = test_rf.drop('label', axis=1)
            test_rf_Y = test_rf['label']

            test_sk_X = test_sk.drop('label', axis=1)
            test_sk_Y = test_sk['label']

            clf.set_params(**gs_model.best_params_)
            clf.fit(train_X, train_Y)

            preds_rf = clf.predict(test_rf_X)
            preds_sk = clf.predict(test_sk_X)

            conf_matrix_rf = confusion_matrix(test_rf_Y, preds_rf)
            conf_matrix_sk = confusion_matrix(test_sk_Y, preds_sk)

            cur_rf_matrices.append(conf_matrix_rf)
            cur_sk_matrices.append(conf_matrix_sk)

        subj_rf_matrices.append(cur_rf_matrices)
        subj_sk_matrices.append(cur_sk_matrices)

    score_list.append(cur_scores)
    best_params.append(subj_best_params)
    rf_matrices.append(subj_rf_matrices)
    sk_matrices.append(subj_sk_matrices)

score_list = np.array(score_list).T.tolist()
for i in range(0, len(names)):
    print("Best scores of " + names[i] + " per subject")
    print("Individual: " + str(score_list[i]))
    print("Mean: " + str(statistics.mean(score_list[i])))
    print("StDev: " + str(statistics.stdev(score_list[i])))
    print("")

avg_rf_matrices = [[np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])]]

avg_sk_matrices = [[np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])],
                   [np.array([[0, 0], [0, 0]]), np.array([[0, 0], [0, 0]])]]

for i in range(0, len(rf_matrices)):
    # Iterate over subjects

    print("Subject label: ", i)

    for j in range(0, len(names)):
        print(names[j])
        print(best_params[i][j])
        print()
        print("RF Matrices: " + str(rf_matrices[i][j]))
        print("SK Matrices: " + str(sk_matrices[i][j]))

        for k in range(0, len(NR_GENUINE_TRAIN)):
            avg_rf_matrices[j][k] = np.add(avg_rf_matrices[j][k], rf_matrices[i][j][k])
            avg_sk_matrices[j][k] = np.add(avg_sk_matrices[j][k], sk_matrices[i][j][k])

print("OVERALL RESULTS:")
print()

for i in range(0, len(names)):
    print(names[i])
    print()

    for j in range(0, len(NR_GENUINE_TRAIN)):
        print("NR. of gen. Signatures:", NR_GENUINE_TRAIN[j])
        cur_avg_sk_matrix = np.divide(avg_sk_matrices[i][j], len(sk_matrices))
        cur_avg_rf_matrix = np.divide(avg_rf_matrices[i][j], len(rf_matrices))

        print("Random Forgeries: ")
        print(cur_avg_rf_matrix)
        tn, fp, fn, tp = cur_avg_rf_matrix.ravel()
        fpr = fp / (fp + tn)
        fnr = fn / (fn + tp)
        print("FPR:", fpr)      # False Acceptance Rate
        print("FNR:", fnr)      # False Rejection Rate
        print()

        print("Skilled Forgeries: ")
        print(cur_avg_sk_matrix)
        tn, fp, fn, tp = cur_avg_sk_matrix.ravel()
        fpr = fp / (fp + tn)
        fnr = fn / (fn + tp)
        print("FPR:", fpr)      # False Acceptance Rate
        print("FNR:", fnr)      # False Rejection Rate
        print()

    print("--------------------")
    print()
