import pandas as pd
import numpy as np
import statistics
from sklearn.metrics import make_scorer
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from util.process_and_train import print_grid_search_results, combine_label_and_genuine, normalize_colwise

# Constants

STATISTICAL_INFO = [
    'devmod', 'genuine', 'pos', 'sex', 'age', 'hand', 'session',
    'scrw', 'scrh', 'denw', 'denh', 'country', 'lang', 'dsize', 'phrel', 'tsQ', 'pID', 'ts'
]
REDUNDANT_INFO = ['vX_', 'vY_']

GLOBAL_INFO = ['duration', 'nrStrokes']

LOCAL_GEOM = [
    'x_', 'y_', 'fc_', 'dX', 'dY',
    'pta', 'dpta', 'pvm', 'dpvm', 'pvmr5',
    'lcr', 'dlcr', 'tam', 'dtam',
    'ddX', 'ddY', 'acs', 'dacs', 'sinacs', 'cosacs',
    'ltwr5', 'ltwr7',]
GLOBAL_GEOM = [
    'startX', 'startY', 'endX', 'endY',
    'minX', 'minY', 'maxX', 'maxY',
    'widthX', 'heightY', 'size', 'maxdX', 'maxdY',
    'mx', 'my', 'mdX', 'mdY',
    'sdx', 'sdy', 'sddX', 'sddY',
    'skx', 'sky', 'skdX', 'skdY',
    'ktx', 'kty', 'ktdX', 'ktdY'
]

LOCAL_TOUCHSIZE = ['touchsize']
GLOBAL_TOUCHSIZE = ['mtouchsize', 'sdtouchsize', 'sktouchsize', 'kttouchsize']

LOCAL_MAG = ['mag']
GLOBAL_MAG = ['mmag', 'sdmag', 'skmag', 'ktmag']

LOCAL_ACC = ['acc']
GLOBAL_ACC = ['macc', 'sdacc', 'skacc', 'ktacc']

LOCAL_GYRO = ['gyro']
GLOBAL_GYRO = ['mgyro', 'sdgyro', 'skgyro', 'ktgyro']

LOCAL_GRAV = ['grav']
GLOBAL_GRAV = ['mgrav', 'sdgrav', 'skgrav', 'ktgrav']

LABEL = ['label']

# Feature shorthands
ALWAYS_REMOVE = STATISTICAL_INFO + REDUNDANT_INFO

GLOBAL_COMBINED = GLOBAL_INFO + GLOBAL_GEOM + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_MOVEMENT = GLOBAL_INFO + GLOBAL_GRAV + GLOBAL_GYRO + GLOBAL_ACC
GLOBAL_XY = GLOBAL_INFO + GLOBAL_GEOM

LOCAL_COMBINED = LOCAL_GEOM + LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_MOVEMENT = LOCAL_ACC + LOCAL_GYRO + LOCAL_GRAV
LOCAL_XY = LOCAL_GEOM

GL_COMBINED = GLOBAL_COMBINED + LOCAL_COMBINED
GL_MOVEMENT = GLOBAL_MOVEMENT + LOCAL_MOVEMENT
GL_XY = GLOBAL_XY + LOCAL_XY

# ----------- Settings ---------------

FILE_DIR = "data/dataset-touchpad/"
FILE_NAME = "dataset.csv"

DIFFERENT_LABELS_FOR_FORGERIES = True

RUN_N_CLASS = False

RUN_SUBJECT_OTHERS = True
TEST_ON_FORGERIES = True

FEATURES = GLOBAL_XY

# ----------- Start of script ----------

dataFrame = pd.read_csv(FILE_DIR + FILE_NAME)

print(dataFrame.info())
print(dataFrame.describe())

number_of_genuine_classes = dataFrame['label'].max() + 1

if DIFFERENT_LABELS_FOR_FORGERIES:
    dataFrame = combine_label_and_genuine(dataFrame, 'label', 'genuine')

# Remove
cols = [c for c in dataFrame.columns if not c.startswith(tuple(ALWAYS_REMOVE))]
dataFrame = dataFrame[cols]

# Select features to use
FEATURES = FEATURES + LABEL
cols = [c for c in dataFrame.columns if c.startswith(tuple(FEATURES))]
dataFrame = dataFrame[cols]

# Test different classifiers
names = ["Nearest Neighbors",
         "Linear SVM",
         "RBF SVM",
         "Decision Tree",
         "Random Forest",
         "Neural Net",
         "AdaBoost",
         "Naive Bayes"
         ]

parameters = [{'n_neighbors': [1, 2, 3, 4, 5, 7, 10]},
              {'kernel': ['linear'], 'C': [0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50, 100]},
              {'kernel': ['rbf'], 'gamma': [0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 2, 5]},
              {'max_depth': [2, 3, 5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None]},
              {'max_depth': [2, 3, 5, 7, 10, 20, 30], 'max_features': ['sqrt', 'log2', None],
               'n_estimators': [20, 50, 100, 200]},
              {'activation': ['logistic', 'tanh', 'relu'], 'solver': ['lbfgs', 'adam'],
               'alpha': [0.0001, 0.001, 0.01, 0.1, 1, 10], 'max_iter': [2000]},
              {'n_estimators': [10, 20, 50, 100], 'learning_rate': [0.1, 0.5, 1.0, 5]},
              {'var_smoothing': [1e-10, 1e-9, 1e-8]}
              ]

classifiers = [
    KNeighborsClassifier(),
    SVC(),
    SVC(),
    DecisionTreeClassifier(),
    RandomForestClassifier(),
    MLPClassifier(),
    AdaBoostClassifier(),
    GaussianNB()
]

scorer = make_scorer(balanced_accuracy_score, adjusted=False)

# TODO: keep validation set aside for actual study data reporting. Don't for now due to limited nr. of samples
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

if RUN_N_CLASS:
    # Subject/Others test only makes sense on random forgeries
    df_no_forgeries = dataFrame.query('label < {0}'.format(number_of_genuine_classes))
    df_no_forgeries = df_no_forgeries.reset_index(drop=True)
    X = df_no_forgeries.drop('label', axis=1)
    y = df_no_forgeries['label']

    for name, params, clf in zip(names, parameters, classifiers):
        gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5, n_jobs=-1)
        gs_model.fit(X, y)

        print_grid_search_results(gs_model, name)

if RUN_SUBJECT_OTHERS:

    score_list = []
    forgery_score_list = []

    feature_importance = [0] * (dataFrame.shape[1]-1)
    feature_importance_skf = [0] * (dataFrame.shape[1]-1)
    count = 0

    for cur_label in range(0, number_of_genuine_classes):
        print("Cur label: " + str(cur_label) + ", Iteration: " + str(count))
        count += 1

        # Dateset without the forgeries for the class under test
        dfNoForgeries = dataFrame.query('label != {0}'.format(cur_label + number_of_genuine_classes))
        dfNoForgeries = dfNoForgeries.reset_index(drop=True)

        # Everything that is not the current label is 0
        dfNoForgeries['label'] = dfNoForgeries['label'].apply(lambda label: 1 if label == cur_label else 0)

        X_no_forg = dfNoForgeries.drop('label', axis=1)
        y_no_forg = dfNoForgeries['label']

        # Dataset with only the forgeries of the class under test
        dfForgeriesOnly = dataFrame.query('label == {0}'.format(cur_label + number_of_genuine_classes))
        dfForgeriesOnly = dfForgeriesOnly.reset_index(drop=True)

        # Make it negative class
        dfForgeriesOnly['label'] = 0

        X_forg_only = dfForgeriesOnly.drop('label', axis=1)
        y_forg_only = dfForgeriesOnly['label']

        cur_scores = []
        cur_forgery_scores = []
        for name, params, clf in zip(names, parameters, classifiers):
            gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5, n_jobs=-1)
            gs_model.fit(X_no_forg, y_no_forg)

            print_grid_search_results(gs_model, name)

            #feature_importance = [(x + y) for x, y in zip(feature_importance, gs_model.best_estimator_.feature_importances_)]
            #col_names = X_no_forg.columns.tolist()
            #print("grid search feature importance (latest average)")

            #indices = np.argsort(feature_importance)[::-1]
            #for f in range(X_no_forg.shape[1]):
            #    print("%d. %s [%s] (%f)" % (f + 1, col_names[indices[f]], indices[f], feature_importance[indices[f]]/count))

            cur_scores.append(gs_model.best_score_)

            if TEST_ON_FORGERIES:
                clf.set_params(**gs_model.best_params_)
                clf.fit(X_no_forg, y_no_forg)

                #feature_importance_skf = [(x + y) for x, y in zip (feature_importance_skf, clf.feature_importances_)]
                #print("skilled forgery feature importance (latest average)")
                #indices = np.argsort(feature_importance_skf)[::-1]
                #for f in range(X_no_forg.shape[1]):
                #    print("%d. %s [%s] (%f)" % (f + 1, col_names[indices[f]], indices[f], feature_importance_skf[indices[f]] / count))

                preds = clf.predict(X_forg_only)
                score = balanced_accuracy_score(y_forg_only, preds)
                print("Score on forgeries only: ", score)

                cur_forgery_scores.append(score)

        score_list.append(cur_scores)
        forgery_score_list.append(cur_forgery_scores)

    score_list = np.array(score_list).T.tolist()
    for i in range(0, len(names)):
        print("Best scores of " + names[i] + " per subject")
        print("Individual: " + str(score_list[i]))
        print("Mean: " + str(statistics.mean(score_list[i])))
        print("StDev: " + str(statistics.stdev(score_list[i])))
        print("")

    if TEST_ON_FORGERIES:
        forgery_score_list = np.array(forgery_score_list).T.tolist()
        for i in range(0, len(names)):
            print("Best scores of " + names[i] + " per subject")
            print("Individual: " + str(forgery_score_list[i]))
            print("Mean: " + str(statistics.mean(forgery_score_list[i])))
            print("StDev: " + str(statistics.stdev(forgery_score_list[i])))
            print("")
