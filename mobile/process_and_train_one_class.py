def warn(*args, **kwargs):          # To remove training label warnings of sklearn
    pass
import warnings
warnings.warn = warn

import pandas as pd
import numpy as np
import statistics
from sklearn.ensemble import IsolationForest
from sklearn.metrics import make_scorer, f1_score, balanced_accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.svm import OneClassSVM
from util.process_and_train import print_grid_search_results, combine_label_and_genuine, normalize_colwise

FILE_DIR = "data/Signatures-Touchpad/"
FILE_NAME = "dataset.csv"

DIFFERENT_LABELS_FOR_FORGERIES = True      # Change drop line before enabling

FORGERY_MODE = False

dataFrame = pd.read_csv(FILE_DIR + FILE_NAME)

print(dataFrame.info())
print(dataFrame.describe())

number_of_genuine_classes = dataFrame['label'].max() + 1

if DIFFERENT_LABELS_FOR_FORGERIES:
    dataFrame = combine_label_and_genuine(dataFrame, 'label', 'genuine')

# Remove unnecessary columns
columns_to_remove = ['devmod', 'genuine', 'mag', 'mmag', 'touchsize', 'mtouchsize']
cols = [c for c in dataFrame.columns if not c.startswith(tuple(columns_to_remove))]
dataFrame = dataFrame[cols]

# Normalization
dataFrame = normalize_colwise(dataFrame)

names = ["Linear One-Class SVM",
         "RBF One-Class SVM",
         "Isolation Forest"]

parameters = [{'kernel': ['linear'], 'nu': [0.01, 0.05, 0.1, 0.25, 0.5]},
              {'kernel': ['rbf'], 'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1],
               'nu': [0.01, 0.05, 0.1, 0.25, 0.5]},
              {'n_estimators': [20, 50, 100, 200, 500], 'max_features': [0.25, 0.5, 0.75, 1.0],
               'contamination': ['auto']}]

classifiers = [OneClassSVM(),
               OneClassSVM(),
               IsolationForest()]

scorer = make_scorer(balanced_accuracy_score, adjusted=False)

score_list = []
TRAIN_SET_SIZE = 0.75
for cur_label in range(0, number_of_genuine_classes):

    if FORGERY_MODE:
        # Only against forgeries
        cur_df = dataFrame.query('label == {0} | label == {1}'.format(cur_label, cur_label+number_of_genuine_classes))
        TRAIN_SET_SIZE = 1          # Use all genuine for training, all forgeries for testing
    else:
        # Only use random forgeries
        cur_df = dataFrame.query('label < {0}'.format(number_of_genuine_classes))
        TRAIN_SET_SIZE = 0.5       # Use half of the genuine samples for training, include other half in test set

    # Select only values from one class to train with
    cur_df['label'] = cur_df['label'].apply(lambda label: 1 if label == cur_label else -1)
    positive = cur_df.query('label == 1')
    train = positive.sample(int(positive.shape[0] * TRAIN_SET_SIZE))
    test = cur_df.drop(train.index)

    # Split into X/y
    y_train = train['label']  # Not needed to train, only one class anyways
    X_train = train.drop('label', 1)
    y_test = test['label']
    X_test = test.drop('label', 1)

    # Test different one-class classifiers
    # CV only trains and tests with positive class, as in reality, there is also nothing else available
    cur_scores = []
    for name, params, clf in zip(names, parameters, classifiers):
        gs_model = GridSearchCV(clf, params, scoring=scorer, cv=5)
        gs_model.fit(X_train, y_train)

        print_grid_search_results(gs_model, name)

        clf.set_params(**gs_model.best_params_)
        clf.fit(X_train)
        preds = clf.predict(X_test)
        score = balanced_accuracy_score(y_test, preds, adjusted=False)
        cur_scores.append(score)
        print("Score: ", score)

    score_list.append(cur_scores)

score_list = np.array(score_list).T.tolist()
for i in range(0, len(names)):
    print("Best scores of " + names[i] + " per subject")
    print("Individual: " + str(score_list[i]))
    print("Mean: " + str(statistics.mean(score_list[i])))
    print("StDev: " + str(statistics.stdev(score_list[i])))
    print("")
